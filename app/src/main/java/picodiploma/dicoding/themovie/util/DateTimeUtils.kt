/*
 * Created by Iqbal Fauzi
 */

package picodiploma.dicoding.themovie.util

import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date

/**
 * Created by Iqbal Fauzi on 13:10 21/06/19
 */
object DateTimeUtils {

    // END EPOCH CONVERTER

    // DATE COMPONENT
    val currentDate: String
        get() = "$currentDay/$currentMonth/$currentYear"

    private val currentTime: String
        get() = "$currentHour:$currentMinute:$currentSecond"

    private val currentDateTime: String
        get() = "$currentDate $currentTime"

    private val currentDay: Int
        get() {
            val calendar = Calendar.getInstance()
            return calendar.get(Calendar.DAY_OF_MONTH)
        }

    // January = 0
    private val currentMonth: Int
        get() {
            val calendar = Calendar.getInstance()
            return calendar.get(Calendar.MONTH) + 1
        }

    private val currentMonthDatePicker: Int
        get() {
            val calendar = Calendar.getInstance()
            return calendar.get(Calendar.MONTH)
        }

    private val currentYear: Int
        get() {
            val calendar = Calendar.getInstance()
            return calendar.get(Calendar.YEAR)
        }

    private val currentHour: Int
        get() {
            val calendar = Calendar.getInstance()
            return calendar.get(Calendar.HOUR_OF_DAY)
        }

    private val currentMinute: Int
        get() {
            val calendar = Calendar.getInstance()
            return calendar.get(Calendar.MINUTE)
        }

    private val currentSecond: Int
        get() {
            val calendar = Calendar.getInstance()
            return calendar.get(Calendar.SECOND)
        }

    // END DATE COMPONENT

    // Calendar CONVERTER

    private val todayInCalendar: Calendar
        get() = Calendar.getInstance()

    private val lastDateOfMonth: Int
        get() {
            val calendar = Calendar.getInstance()
            return calendar.getActualMaximum(Calendar.DAY_OF_MONTH)
        }

    // EPOCH CONVERTER
    fun epochToDate(epoch: Long): String {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = epoch * 1000
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH) + 1
        val year = calendar.get(Calendar.YEAR)

        return (if (day < 10) "0$day" else day).toString() + "/" + (if (month < 10) "0$month" else month) + "/" + year
    }

    fun epochToTime(epoch: Long): String {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = epoch * 1000
        val hour = calendar.get(Calendar.HOUR_OF_DAY)
        val minute = calendar.get(Calendar.MINUTE)

        return (if (hour < 10) "0$hour" else hour).toString() + ":" + if (minute < 10) "0$minute" else minute
    }

    fun epochToDateTime(epoch: Long): String {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = epoch * 1000
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH) + 1
        val year = calendar.get(Calendar.YEAR)
        val hour = calendar.get(Calendar.HOUR_OF_DAY)
        val minute = calendar.get(Calendar.MINUTE)

        return ((if (day < 10) "0$day" else day).toString() + "/" + (if (month < 10) "0$month" else month) + "/" + year + " "
                + (if (hour < 10) "0$hour" else hour) + ":" + if (minute < 10) "0$minute" else minute)
    }

    fun epochToHumanDate(epoch: Long): String {
        return SimpleDateFormat("EEEE, dd MMMM yyyy", CommonUtils.locale)
            .format(Date(epoch * 1000))
    }

    fun epochToReadableDate(epoch: Long): String {
        return SimpleDateFormat("dd MMM yyyy", CommonUtils.localeID)
            .format(Date(epoch * 1000))
    }

    fun epochToHumanDateTime(epoch: Long): String {
        return SimpleDateFormat("EEEE, dd MMMM yyyy HH:mm", CommonUtils.localeID)
            .format(Date(epoch * 1000))
    }

    fun convertToHumanDate(value: String): String {
        var humanDate = ""
        val date: Array<String>

        date = if (value.contains(":")) {
            val dateTime = value.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            dateTime[0].split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        } else {
            value.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        }

        val dateInEpoch = dateToEpoch(date[2] + "/" + date[1] + "/" + date[0])
        humanDate = epochToHumanDate(dateInEpoch)

        return humanDate
    }

    fun convertToReadableDate(value: String): String {
        var humanDate = ""
        val date: Array<String>

        date = if (value.contains(":")) {
            val dateTime = value.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            dateTime[0].split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        } else {
            value.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        }

        val dateInEpoch = dateToEpoch(date[2] + "/" + date[1] + "/" + date[0])
        humanDate = epochToReadableDate(dateInEpoch)

        return humanDate
    }

    fun convertToHumanDateTime(value: String): String {
        val humanDate: String

        val dateTime = value.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val date = dateTime[0].split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val time = dateTime[1].split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

        val dateInEpoch = dateTimeToEpoch(
            date[2] + "/" + date[1] + "/" + date[0] + " "
                    + time[0] + ":" + time[1]
        )
        humanDate = epochToHumanDateTime(dateInEpoch)

        return humanDate
    }

    fun dateToEpoch(date: String): Long {
        val splitDate = CommonUtils.getSplittedString(date, "/")

        val calendar = Calendar.getInstance()
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(splitDate[0]))
        calendar.set(Calendar.MONTH, Integer.parseInt(splitDate[1]) - 1)
        calendar.set(Calendar.YEAR, Integer.parseInt(splitDate[2]))
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)

        return calendar.timeInMillis / 1000
    }

    fun dateTimeToEpoch(dateTime: String): Long {
        val splitDateTime = CommonUtils.getSplittedString(dateTime, " ")
        val splitDate = CommonUtils.getSplittedString(splitDateTime[0], "/")
        val splitTime = CommonUtils.getSplittedString(splitDateTime[1], ":")

        val calendar = Calendar.getInstance()
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(splitDate[0]))
        calendar.set(Calendar.MONTH, Integer.parseInt(splitDate[1]) - 1)
        calendar.set(Calendar.YEAR, Integer.parseInt(splitDate[2]))
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(splitTime[0]))
        calendar.set(Calendar.MINUTE, Integer.parseInt(splitTime[1]))

        return calendar.timeInMillis / 1000
    }

    fun getDateTimeInCalendar(day: Int?, month: Int?, year: Int?, hour: Int?, minute: Int?): Calendar {
        val calendar = Calendar.getInstance()
        day?.let { calendar.set(Calendar.DAY_OF_MONTH, it) }
        month?.minus(1)?.let { calendar.set(Calendar.MONTH, it) }
        year?.let { calendar.set(Calendar.YEAR, it) }
        hour?.let { calendar.set(Calendar.HOUR_OF_DAY, it) }
        minute?.let { calendar.set(Calendar.MINUTE, it) }

        return calendar
    }

    fun getDateInCalendar(day: Int?, month: Int?, year: Int?): Calendar {
        val calendar = Calendar.getInstance()
        day?.let { calendar.set(Calendar.DAY_OF_MONTH, it) }
        month?.minus(1)?.let { calendar.set(Calendar.MONTH, it) }
        year?.let { calendar.set(Calendar.YEAR, it) }

        return calendar
    }

    //format date 28/11/1995
    fun dateToCalendar(date: String): Calendar {
        val splitDate = CommonUtils.getSplittedString(date, "/")
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(splitDate[0]))
        calendar.set(Calendar.MONTH, Integer.parseInt(splitDate[1]) - 1)
        calendar.set(Calendar.YEAR, Integer.parseInt(splitDate[2]))

        return calendar
    }

    //format datetime 28/11/1995 19:00
    fun getDateTimeInCalendar(dateTime: String): Calendar {
        val datetimeSplit = CommonUtils.getSplittedString(dateTime, " ")
        val dateSplit = CommonUtils.getSplittedString(datetimeSplit[0], "/")
        val timeSplit = CommonUtils.getSplittedString(datetimeSplit[1], ":")

        val calendar = Calendar.getInstance()
        calendar.set(Calendar.YEAR, Integer.parseInt(dateSplit[2]))
        calendar.set(Calendar.MONTH, Integer.parseInt(dateSplit[1]) - 1)
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateSplit[0]))
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeSplit[0]))
        calendar.set(Calendar.MINUTE, Integer.parseInt(timeSplit[1]))

        return calendar
    }

    fun getAddedDayCalendar(date: String, addDay: Int): Calendar {
        val calendar = dateToCalendar(date)
        calendar.add(Calendar.DAY_OF_MONTH, addDay)

        return calendar
    }

    fun getAddedMonthCalendar(date: String, addMonth: Int): Calendar {
        val calendar = dateToCalendar(date)
        calendar.add(Calendar.MONTH, addMonth)

        return calendar
    }

    fun getAddedYearCalendar(date: String, addYear: Int): Calendar {
        val calendar = dateToCalendar(date)
        calendar.add(Calendar.YEAR, addYear)

        return calendar
    }

    fun countDate(tglAwal: Long?, tglAkhir: Long?): Long? {
        val tglMulai = tglAwal?.let { epochToDate(it) }
        val tglSelesai = tglAkhir?.let { epochToDate(it) }

        val startCalendar = dateToCalendar(tglMulai.toString())
        val endCalendar = dateToCalendar(tglSelesai.toString())

        val dateInMilis = startCalendar.timeInMillis - endCalendar.timeInMillis

        return dateInMilis / (24 * 60 * 60 * 1000)
    }

    // END Calendar CONVERTER

    fun getMonth(position: Int): String {
        val months = arrayOf(
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "Oktober",
            "November",
            "December"
        )

        return months[position]
    }

    fun getDayOfWeek(position: Int): String {
        val dayOfWeek = arrayOf("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu")

        return dayOfWeek[position]
    }

    fun getHour(hour: Int?): String {
        var textHour = ""

        if (hour != null) {
            textHour = if (hour < 10) {
                "0$hour"
            } else {
                hour.toString()
            }
        }

        return textHour
    }

    fun getMinute(minute: Int?): String {
        var textMinute = ""

        if (minute != null) {
            textMinute = if (minute < 10) {
                "0$minute"
            } else {
                minute.toString()
            }
        }

        return textMinute
    }
}
