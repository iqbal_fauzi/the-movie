/*
 * Created by Iqbal Fauzi
 */

package picodiploma.dicoding.themovie.util

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityManager
import android.content.Context
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.os.Build
import android.provider.Settings
import com.google.android.material.floatingactionbutton.FloatingActionButton
import android.text.Html
import android.text.Spanned
import android.view.View
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.NumberFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Created by Iqbal Fauzi on 13:44 20/06/19
 */
object CommonUtils {

    private val TAG = "CommonUtils"
    private const val EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"

    val localeID: Locale
        get() = Locale("in", "ID")

    val locale: Locale = Locale.getDefault()

    fun sortStringContainsNumber(values: ArrayList<String>): ArrayList<String> {
        Collections.sort(values, object : Comparator<String> {
            override fun compare(o1: String, o2: String): Int {
                return extractInt(o1) - extractInt(o2)
            }

            fun extractInt(s: String): Int {
                val num = s.replace("\\D".toRegex(), "")
                // return 0 if no digits found
                return if (num.isEmpty()) 0 else Integer.parseInt(num)
            }
        })

        return values
    }

    @SuppressLint("all")
    fun getDeviceId(context: Context): String {
        return Settings.Secure.getString(context.contentResolver, Settings
                .Secure.ANDROID_ID)
    }

    fun isEmailValid(email: String): Boolean {
        val pattern: Pattern = Pattern.compile(EMAIL_PATTERN)
        val matcher: Matcher

        matcher = pattern.matcher(email)
        return matcher.matches()
    }

    fun isJSONValid(test: String?): Boolean {

        if (test == null || test.isEmpty()) {
            return false
        }

        try {
            JSONObject(test)
        } catch (ex: JSONException) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                JSONArray(test)
            } catch (ex1: JSONException) {
                return false
            }

        }

        return true
    }

    fun autoHideFab(fabView: FloatingActionButton, dy: Int) {
        if (dy > 0 && fabView.visibility == View.VISIBLE) {
            fabView.hide()
        } else if (dy < 0 && fabView.visibility != View.VISIBLE) {
            fabView.show()
        }
    }

    fun getPriceFormat(locale: Locale, price: Double): String {
        val currencyFormat = NumberFormat.getCurrencyInstance(locale)

        return currencyFormat.format(price)
    }

    fun getSplittedString(text: String, regex: String): Array<String> {
        return text.split(regex.toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
    }

    fun getColor(context: Context, id: Int): Int {
        return if (Build.VERSION.SDK_INT >= 23) {
            context.resources.getColor(id, context.theme)
        } else context.resources.getColor(id)

    }

    fun getDrawable(context: Context, id: Int): Drawable {
        return if (Build.VERSION.SDK_INT >= 21) {
            context.resources.getDrawable(id, context.theme)
        } else context.resources.getDrawable(id)
    }

    fun getTextFromHtml(html: String): Spanned {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT)
        } else {
            Html.fromHtml(html)
        }
    }

    fun setTypeface(context: Context, font: String): Typeface {
        return Typeface.createFromAsset(context.assets, font)
    }

    fun isServiceRunning(activity: Activity, serviceClass: Class<*>): Boolean {
        val manager = activity.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }

    fun isNetworkConnected(context: Context): Boolean {
        val cm = context.getSystemService(Context
                .CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }
}
