package picodiploma.dicoding.themovie.util

import android.content.Context
import android.content.Intent
import android.graphics.Color
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.widget.AppCompatButton
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import picodiploma.dicoding.themovie.R

/**
 * Created by Iqbal Fauzi on 14:53 23/06/19
 */

fun Context.toastLong(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

fun Context.toastShort(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun ProgressBar.show() {
    visibility = View.VISIBLE
}

fun ProgressBar.hide() {
    visibility = View.GONE
}

fun AppCompatButton.show() {
    visibility = View.VISIBLE
}

fun AppCompatButton.hide() {
    visibility = View.GONE
}

fun View.show() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.hide() {
    visibility = View.GONE
}

//fun Context.goToActivity(targetClass: Class<*>) {
//    startActivity(Intent(this, targetClass))
//}

fun View.snackBar(message: String) {
    Snackbar.make(this, message, Snackbar.LENGTH_LONG).show()
}

fun View.snackBarRed(message: String) {
    val snackbar = Snackbar.make(this, message, Snackbar.LENGTH_LONG)
    val sbView = snackbar.view
    val textView = sbView.findViewById<View>(R.id.snackbar_text) as TextView
    textView.setTextColor(Color.WHITE)
    sbView.setBackgroundColor(resources.getColor(R.color.colorPrimary))
    snackbar.show()
}

fun View.showKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
}

fun View.hideKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}