package picodiploma.dicoding.themovie.di

import androidx.room.Room
import org.koin.dsl.module
import picodiploma.dicoding.themovie.data.local.MovieDatabase
import picodiploma.dicoding.themovie.data.local.repository.FavoriteMovieRepository
import picodiploma.dicoding.themovie.data.local.repository.FavoriteTvRepository
import picodiploma.dicoding.themovie.data.local.repository.MovieRepository
import picodiploma.dicoding.themovie.data.local.repository.TvRepository

/**
 * Created by Iqbal Fauzi on 15:38 10/08/19
 */
val localDataModule = module {

    single {
        Room.databaseBuilder(get(), MovieDatabase::class.java, "movieDatabase.db")
            .fallbackToDestructiveMigration().build()
    }
    single { get<MovieDatabase>().movieDao() }
    single { get<MovieDatabase>().tvDao() }
    single { get<MovieDatabase>().favoriteMovieDao() }
    single { get<MovieDatabase>().favoriteTvDao() }
    single { MovieRepository(get()) }
    single { TvRepository(get()) }
    single { FavoriteMovieRepository(get()) }
    single { FavoriteTvRepository(get()) }
}