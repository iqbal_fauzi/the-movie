package picodiploma.dicoding.themovie.di

import org.koin.dsl.module
import picodiploma.dicoding.themovie.data.network.NetworkConfig

/**
 * Created by Iqbal Fauzi on 19:46 13/07/19
 */
val networkModule = module {

    single { NetworkConfig() }

}