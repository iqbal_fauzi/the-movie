package picodiploma.dicoding.themovie.di

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import picodiploma.dicoding.themovie.viewmodel.*

/**
 * Created by Iqbal Fauzi on 19:50 13/07/19
 */
val viewModelModule = module {

    viewModel { MovieViewModel(get(), get()) }
    viewModel { TvViewModel(get()) }
    viewModel { DetailViewModel(get(),get()) }
    viewModel { FavoriteViewModel(get(), get()) }
    viewModel { SearchViewModel(get(),get()) }

}