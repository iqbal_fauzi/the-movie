package picodiploma.dicoding.themovie.ui.features.main.fragments

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import picodiploma.dicoding.themovie.R
import picodiploma.dicoding.themovie.base.BaseFragmentVM
import picodiploma.dicoding.themovie.data.local.FavoriteMovie
import picodiploma.dicoding.themovie.data.response.MovieResponse
import picodiploma.dicoding.themovie.databinding.FragmentMovieBinding
import picodiploma.dicoding.themovie.ui.features.adapter.MovieAdapter
import picodiploma.dicoding.themovie.ui.features.details.DetailActivity
import picodiploma.dicoding.themovie.ui.features.favorite.FavoriteActivity
import picodiploma.dicoding.themovie.ui.features.reminder.ReminderSettingActivity
import picodiploma.dicoding.themovie.ui.features.search.SearchActivity
import picodiploma.dicoding.themovie.ui.listener.OnFavoriteCheckListener
import picodiploma.dicoding.themovie.ui.listener.OnItemClickListener
import picodiploma.dicoding.themovie.util.hide
import picodiploma.dicoding.themovie.util.show
import picodiploma.dicoding.themovie.util.snackBar
import picodiploma.dicoding.themovie.util.toastShort
import picodiploma.dicoding.themovie.viewmodel.MovieViewModel

class MovieFragment : BaseFragmentVM<MovieViewModel, FragmentMovieBinding>(MovieViewModel::class) {

    private lateinit var movieAdapter: MovieAdapter
    private lateinit var arrayList: ArrayList<MovieResponse.Movies>

    override fun onViewInitialized(savedInstanceState: Bundle?) {
        onRequest()
        setHasOptionsMenu(true)
        with(viewModel) {
            getMovieNowPlaying()
            databinding.tvTitle1.text = resources.getString(R.string.now_playing)
        }
        arrayList = ArrayList()
        setRecycler()
        showErrorMessage()
    }


    private fun showErrorMessage() {
        with(viewModel) {
            message.observe(this@MovieFragment, Observer {
                databinding.root.snackBar(it)
            })

            handleError.observe(this@MovieFragment, Observer {
                handleError(it)
            })
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_search -> {
                val intentSearch = Intent(activity, SearchActivity::class.java)
                intentSearch.putExtra(SearchActivity.TYPE, "movie")
                startActivity(intentSearch)
                true
            }
            R.id.favorite_menu -> {
                startActivity(Intent(activity, FavoriteActivity::class.java))
                true
            }
            R.id.action_change_settings -> {
                val mIntent = Intent(Settings.ACTION_LOCALE_SETTINGS)
                startActivity(mIntent)
                true
            }
            R.id.action_reminder_settings -> {
                startActivity(Intent(activity, ReminderSettingActivity::class.java))
                true
            }
            else -> true
        }
    }

    private fun setRecycler() {
        //get Now Playing Data
        viewModel.movieData.observe(this, Observer { response ->
            with(databinding) {
                if (response.isNullOrEmpty()) {
                    tvEmpty.show()
                    rv1.hide()
                } else {
                    rv1.apply {
                        show()
                        tvEmpty.hide()
                        arrayList = response as ArrayList<MovieResponse.Movies>
                        layoutManager = GridLayoutManager(mContext, 2)
                        movieAdapter = MovieAdapter(false, arrayList, object : OnItemClickListener {
                            override fun onItemClick(itemView: View, position: Int) {
                                val data = Intent(mContext, DetailActivity::class.java)
                                data.putExtra(DetailActivity.ITEM_TYPE, "movie")
                                data.putExtra(DetailActivity.EXTRA_DATA, response[position])
                                startActivity(data)
                            }

                        }, object : OnFavoriteCheckListener {
                            override fun onChecked(itemView: View, position: Int) {
                                val favoriteMovie = FavoriteMovie(response[position].id.toLong(), response[position].releaseDate, response[position].title, response[position].posterPath)
                                viewModel.setFavoriteMovie(favoriteMovie)
                                mContext?.toastShort("Data added to favorite")
                            }

                            override fun onUnChecked(itemView: View, position: Int) {
                                val favoriteMovie = FavoriteMovie(response[position].id.toLong(), response[position].releaseDate, response[position].title, response[position].posterPath)
                                viewModel.removeFavoriteMovie(favoriteMovie)
                                mContext?.toastShort("Data deleted from favorite")
                            }

                        })
                        adapter = movieAdapter
                    }

                }
            }
        })

    }

    private fun onRequest() {
        with(databinding) {
            //request now Playing Movie
            viewModel.onRequestData.observe(this@MovieFragment, Observer {
                if (it) {
                    pbLoading.show()
                } else pbLoading.hide()
            })
        }
    }

    override fun getResourceLayout(): Int {
        return R.layout.fragment_movie
    }
}
