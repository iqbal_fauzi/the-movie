package picodiploma.dicoding.themovie.ui.features.reminder

import android.os.Bundle
import kotlinx.android.synthetic.main.activity_reminder_setting.*
import picodiploma.dicoding.themovie.R
import picodiploma.dicoding.themovie.base.BaseActivityVM
import picodiploma.dicoding.themovie.databinding.ActivityReminderSettingBinding
import picodiploma.dicoding.themovie.viewmodel.MovieViewModel

class ReminderSettingActivity : BaseActivityVM<MovieViewModel, ActivityReminderSettingBinding>(MovieViewModel::class) {

    override fun onInitializedView(savedInstanceState: Bundle?) {
        initToolbar()
        supportFragmentManager.beginTransaction().add(R.id.fl, ReminderSettingFragment()).commit()
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar_setting)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = resources.getString(R.string.setting_reminder)
    }

    override fun getLayout(): Int {
        return R.layout.activity_reminder_setting
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}
