package picodiploma.dicoding.themovie.ui.features.favorite.fragments


import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import picodiploma.dicoding.themovie.R
import picodiploma.dicoding.themovie.base.BaseFragmentVM
import picodiploma.dicoding.themovie.data.local.FavoriteMovie
import picodiploma.dicoding.themovie.databinding.FragmentFavoriteBinding
import picodiploma.dicoding.themovie.ui.features.adapter.FavoriteMovieAdapter
import picodiploma.dicoding.themovie.ui.features.details.DetailActivity
import picodiploma.dicoding.themovie.ui.features.favorite.FavoriteActivity
import picodiploma.dicoding.themovie.ui.listener.OnFavoriteCheckListener
import picodiploma.dicoding.themovie.ui.listener.OnItemClickListener
import picodiploma.dicoding.themovie.util.hide
import picodiploma.dicoding.themovie.util.show
import picodiploma.dicoding.themovie.util.snackBar
import picodiploma.dicoding.themovie.util.toastShort
import picodiploma.dicoding.themovie.viewmodel.FavoriteViewModel


class FavoriteMovieFragment : BaseFragmentVM<FavoriteViewModel, FragmentFavoriteBinding>(FavoriteViewModel::class) {

    private lateinit var movieAdapter: FavoriteMovieAdapter
    private lateinit var arrayLists: ArrayList<FavoriteMovie>

    override fun onViewInitialized(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        viewModel.getAllFavouriteMovie()
        arrayLists = ArrayList()
        setRecycler()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.options_menu, menu)
        val favorite = menu.findItem(R.id.favorite_menu)
        val setting = menu.findItem(R.id.action_change_settings)
        setting.isVisible = false
        favorite.isVisible = false
        val mSearch = menu.findItem(R.id.action_search)
        val mSearchView = mSearch.actionView as SearchView
        mSearchView.queryHint = "Search Movie"
        mSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                movieAdapter.filter.filter(query)
                return true
            }

            override fun onQueryTextChange(query: String?): Boolean {
                movieAdapter.filter.filter(query)
                return true
            }
        })
        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.favorite_menu -> {
                startActivity(Intent(activity, FavoriteActivity::class.java))
                true
            }
            R.id.action_change_settings -> {
                val mIntent = Intent(Settings.ACTION_LOCALE_SETTINGS)
                startActivity(mIntent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setRecycler() {
        //get Favorite Movie Data
        viewModel.favoriteMovieData.observe(this, Observer { values ->
            with(databinding) {
                rvFavorite.apply {
                    if (values.isNotEmpty()) {
                        show()
                        arrayLists = values as ArrayList<FavoriteMovie>
                        layoutManager = GridLayoutManager(mContext, 2)
                        movieAdapter = FavoriteMovieAdapter(false, arrayLists, object : OnItemClickListener {
                            override fun onItemClick(itemView: View, position: Int) {
                                val data = Intent(mContext, DetailActivity::class.java)
                                data.putExtra(DetailActivity.ITEM_TYPE, "favorite_movie")
                                data.putExtra(DetailActivity.EXTRA_DATA, values[position])
                                startActivity(data)
                            }
                        }, object : OnFavoriteCheckListener {
                            override fun onChecked(itemView: View, position: Int) {
                                //empty function
                            }

                            override fun onUnChecked(itemView: View, position: Int) {
                                viewModel.removeFavoriteMovie(values[position])
                                mContext?.toastShort("Data deleted from favorite")
                            }
                        })
                        adapter = movieAdapter
                    } else {
                        tvEmpty.show()
                        hide()
                    }
                }
            }
        })
    }

    override fun getResourceLayout(): Int {
        return R.layout.fragment_favorite
    }
}
