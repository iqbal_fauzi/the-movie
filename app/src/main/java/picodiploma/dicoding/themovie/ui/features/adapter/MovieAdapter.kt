package picodiploma.dicoding.themovie.ui.features.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.BounceInterpolator
import android.view.animation.ScaleAnimation
import android.widget.CompoundButton
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_movie.view.*
import picodiploma.dicoding.themovie.R
import picodiploma.dicoding.themovie.data.response.MovieResponse
import picodiploma.dicoding.themovie.databinding.ItemMovieBinding
import picodiploma.dicoding.themovie.ui.listener.OnFavoriteCheckListener
import picodiploma.dicoding.themovie.ui.listener.OnItemClickListener
import picodiploma.dicoding.themovie.util.DateTimeUtils

class MovieAdapter(private val limit: Boolean, list: List<MovieResponse.Movies>, val listener: OnItemClickListener, val favoriteListener: OnFavoriteCheckListener)
    : RecyclerView.Adapter<MovieAdapter.Item>(), Filterable {

    private val movieArrayList: List<MovieResponse.Movies> = list
    private var movieArrayListFiltered: List<MovieResponse.Movies>

    init {
        this.movieArrayListFiltered = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Item {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemMovieBinding.inflate(inflater)
        return Item(binding)
    }

    override fun getItemCount(): Int {
        return if (limit) {
            5
        } else movieArrayListFiltered.size
    }

    override fun onBindViewHolder(holder: Item, position: Int) = holder.bind(movieArrayListFiltered[position])

    inner class Item(private val binding: ItemMovieBinding) : RecyclerView.ViewHolder(binding.root) {
        init {
            with(binding) {
                root.setOnClickListener {
                    listener.onItemClick(it, adapterPosition)
                }
                btnFavorite.setOnCheckedChangeListener(object : View.OnClickListener, CompoundButton.OnCheckedChangeListener {
                    override fun onClick(v: View?) {

                    }

                    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                        val scaleAnimation = ScaleAnimation(0.7f, 1.0f, 0.7f, 1.0f, Animation.RELATIVE_TO_SELF, 0.7f, Animation.RELATIVE_TO_SELF, 0.7f)
                        scaleAnimation.duration = 500
                        val bounceInterpolator = BounceInterpolator()
                        scaleAnimation.interpolator = bounceInterpolator
                        btnFavorite.startAnimation(scaleAnimation)
                        if (isChecked) {
                            favoriteListener.onChecked(root, adapterPosition)
                        } else {
                            favoriteListener.onUnChecked(root, adapterPosition)
                        }
                    }
                })

            }
        }

        fun bind(item: MovieResponse.Movies) {
            with(binding) {
                tvTitle.text = item.title
                val releaseDate = if (item.releaseDate.isEmpty()) {
                    "-"
                } else {
                    item.releaseDate.let { DateTimeUtils.convertToHumanDate(it) }
                }
                tvDate.text = releaseDate
                val url = "https://image.tmdb.org/t/p/w600_and_h900_bestv2"
                val poster = url + item.posterPath
                if (item.posterPath == null) {
                    Picasso.get().load(R.drawable.no_image).into(ivPoster)
                } else {
                    Picasso.get().load(poster).into(ivPoster)
                }
            }
        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence?): FilterResults {
                val charString = charSequence.toString()
                movieArrayListFiltered = if (charString.isEmpty()) {
                    movieArrayList
                } else {
                    val filteredList = ArrayList<MovieResponse.Movies>()
                    for (row in movieArrayList) {
                        if (row.title.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row)
                        }
                    }
                    filteredList
                }
                val filterResult = FilterResults()
                filterResult.values = movieArrayListFiltered
                return filterResult
            }

            override fun publishResults(charSequence: CharSequence?, results: FilterResults) {
                movieArrayListFiltered = results.values as ArrayList<MovieResponse.Movies>
                notifyDataSetChanged()
            }

        }
    }

}
