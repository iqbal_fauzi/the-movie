package picodiploma.dicoding.themovie.ui.features.main

import android.os.Bundle
import picodiploma.dicoding.themovie.R
import picodiploma.dicoding.themovie.base.BaseActivityVM
import picodiploma.dicoding.themovie.databinding.ActivityMainBinding
import picodiploma.dicoding.themovie.ui.features.adapter.SectionsPagerAdapter
import picodiploma.dicoding.themovie.ui.features.main.fragments.MovieFragment
import picodiploma.dicoding.themovie.ui.features.main.fragments.TvFragment
import picodiploma.dicoding.themovie.viewmodel.MovieViewModel

class MainActivity : BaseActivityVM<MovieViewModel, ActivityMainBinding>(MovieViewModel::class) {

    override fun onInitializedView(savedInstanceState: Bundle?) {
        setSupportActionBar(databinding.toolbar)
        setStatePagerAdapter()
    }

    override fun getLayout(): Int {
        return R.layout.activity_main
    }

    private fun setStatePagerAdapter() {
        with(databinding) {
            val tabAdapter = SectionsPagerAdapter(supportFragmentManager)
            tabAdapter.addFragment(MovieFragment(), getString(R.string.movie))
            tabAdapter.addFragment(TvFragment(), getString(R.string.tv_show))
            viewPager.adapter = tabAdapter
            tabs.setupWithViewPager(viewPager, true)
        }

    }
}