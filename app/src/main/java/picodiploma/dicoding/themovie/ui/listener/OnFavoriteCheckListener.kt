package picodiploma.dicoding.themovie.ui.listener

import android.view.View

/**
 * Created by Iqbal Fauzi on 15:03 14/07/19
 */
interface OnFavoriteCheckListener {
    fun onChecked(itemView: View, position: Int)
    fun onUnChecked(itemView: View, position: Int)
}