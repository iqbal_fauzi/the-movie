package picodiploma.dicoding.themovie.ui.features.details

import android.os.Build
import android.os.Bundle
import android.view.WindowManager
import androidx.lifecycle.Observer
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail.*
import picodiploma.dicoding.themovie.R
import picodiploma.dicoding.themovie.base.BaseActivityVM
import picodiploma.dicoding.themovie.data.local.FavoriteMovie
import picodiploma.dicoding.themovie.data.local.FavoriteTv
import picodiploma.dicoding.themovie.data.response.MovieResponse
import picodiploma.dicoding.themovie.data.response.TvResponse
import picodiploma.dicoding.themovie.databinding.ActivityDetailBinding
import picodiploma.dicoding.themovie.util.DateTimeUtils
import picodiploma.dicoding.themovie.util.hide
import picodiploma.dicoding.themovie.util.show
import picodiploma.dicoding.themovie.util.snackBar
import picodiploma.dicoding.themovie.viewmodel.DetailViewModel

class DetailActivity : BaseActivityVM<DetailViewModel, ActivityDetailBinding>(DetailViewModel::class) {

    override fun onInitializedView(savedInstanceState: Bundle?) {
        checkNotchScreen()
        initToolbar()
        when (intent.getStringExtra(ITEM_TYPE)) {
            "movie" -> {
                val movieItem = intent.getParcelableExtra<MovieResponse.Movies>(EXTRA_DATA)
                viewModel.getMovieDetail(movieItem?.id.toString())
            }
            "favorite_movie" -> {
                val favoriteMovie = intent.getParcelableExtra<FavoriteMovie>(EXTRA_DATA)
                viewModel.getMovieDetail(favoriteMovie?.id.toString())
            }
            "favorite_tv" -> {
                val favoriteTv = intent.getParcelableExtra<FavoriteTv>(EXTRA_DATA)
                viewModel.getTvDetail(favoriteTv?.id.toString())
            }
            else -> {
                val tvItem = intent.getParcelableExtra<TvResponse.TvShow>(EXTRA_DATA)
                viewModel.getTvDetail(tvItem?.id.toString())
            }
        }
        onRequest()
        setData()
        showErrorMessage()
    }

    private fun showErrorMessage() {
        with(viewModel) {
            message.observe(this@DetailActivity, Observer {
                databinding.root.snackBar(it)
            })

            handleError.observe(this@DetailActivity, Observer {
                handleError(it)
            })
        }
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = ""
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    private fun onRequest() {
        viewModel.onRequest.observe(this, Observer {
            it.let {
                if (it) {
                    progress_poster.show()
                    progress_backdrop.show()
                } else {
                    progress_poster.hide()
                    progress_backdrop.hide()
                }
            }
        })
    }

    override fun getLayout(): Int {
        return R.layout.activity_detail
    }

    private fun checkNotchScreen() {
        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN)
        val attrib = window.attributes
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            attrib.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES
        }
    }

    private fun setData() {
        when (intent.getStringExtra(ITEM_TYPE)) {
            "movie" -> viewModel.movieDetailData.observe(this, Observer { response ->
                with(databinding) {
                    tvTitleDetail.text = response.title
                    tvDateDetail.text = response.releaseDate.let { DateTimeUtils.convertToHumanDate(it.toString()) }
                    tvScore.text = response.voteAverage.toString()
                    tvOverview.text = response.overview
                    val url = "https://image.tmdb.org/t/p/w600_and_h900_bestv2"
                    val poster = url + response.posterPath
                    val backdrop = url + response.backdropPath
                    Picasso.get().load(poster).into(ivPoster)
                    Picasso.get().load(backdrop).into(ivBackdrop)
                }
            })
            "favorite_movie" -> {
                viewModel.movieDetailData.observe(this, Observer { response ->
                    with(databinding) {
                        tvTitleDetail.text = response.title
                        tvDateDetail.text = response.releaseDate.let { DateTimeUtils.convertToHumanDate(it.toString()) }
                        tvScore.text = response.voteAverage.toString()
                        tvOverview.text = response.overview
                        val url = "https://image.tmdb.org/t/p/w600_and_h900_bestv2"
                        val poster = url + response.posterPath
                        val backdrop = url + response.backdropPath
                        Picasso.get().load(poster).into(ivPoster)
                        Picasso.get().load(backdrop).into(ivBackdrop)
                    }
                })
            }
            "favorite_tv" -> {
                viewModel.tvDetailData.observe(this, Observer { response ->
                    with(databinding) {
                        tvTitleDetail.text = response.name
                        tvDateDetail.text = response.firstAirDate.let { DateTimeUtils.convertToHumanDate(it.toString()) }
                        tvScore.text = response.voteAverage.toString()
                        tvOverview.text = response.overview
                        val url = "https://image.tmdb.org/t/p/w600_and_h900_bestv2"
                        val poster = url + response.posterPath
                        val backdrop = url + response.backdropPath
                        Picasso.get().load(poster).into(ivPoster)
                        Picasso.get().load(backdrop).into(ivBackdrop)
                    }
                })
            }
            else -> viewModel.tvDetailData.observe(this, Observer { response ->
                with(databinding) {
                    tvTitleDetail.text = response.name
                    tvDateDetail.text = response.firstAirDate.let { DateTimeUtils.convertToHumanDate(it.toString()) }
                    tvScore.text = response.voteAverage.toString()
                    tvOverview.text = response.overview
                    val url = "https://image.tmdb.org/t/p/w600_and_h900_bestv2"
                    val poster = url + response.posterPath
                    val backdrop = url + response.backdropPath
                    Picasso.get().load(poster).into(ivPoster)
                    Picasso.get().load(backdrop).into(ivBackdrop)
                }
            })
        }
    }

    companion object {
        const val ITEM_TYPE = "item_type"
        const val EXTRA_DATA = "extra_data"
    }

}
