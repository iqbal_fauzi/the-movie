package picodiploma.dicoding.themovie.ui.features.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.BounceInterpolator
import android.view.animation.ScaleAnimation
import android.widget.CompoundButton
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import picodiploma.dicoding.themovie.R
import picodiploma.dicoding.themovie.data.response.TvResponse
import picodiploma.dicoding.themovie.databinding.ItemMovieBinding
import picodiploma.dicoding.themovie.ui.listener.OnFavoriteCheckListener
import picodiploma.dicoding.themovie.ui.listener.OnItemClickListener
import picodiploma.dicoding.themovie.util.DateTimeUtils

class TvAdapter(private val limit: Boolean, list: List<TvResponse.TvShow>, val listener: OnItemClickListener, val favoriteListener: OnFavoriteCheckListener)
    : RecyclerView.Adapter<TvAdapter.Item>(), Filterable {

    private val tvArrayList: List<TvResponse.TvShow> = list
    private var tvArrayListFiltered: List<TvResponse.TvShow> = list

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Item {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemMovieBinding.inflate(inflater)
        return Item(binding)
    }

    override fun getItemCount(): Int {
        return if (limit) {
            5
        } else tvArrayListFiltered.size
    }

    override fun onBindViewHolder(holder: Item, position: Int) = holder.bind(tvArrayListFiltered[position])

    inner class Item(private val binding: ItemMovieBinding) : RecyclerView.ViewHolder(binding.root) {
        init {
            with(binding) {
                root.setOnClickListener {
                    listener.onItemClick(itemView, adapterPosition)
                }
                btnFavorite.setOnCheckedChangeListener(object : View.OnClickListener, CompoundButton.OnCheckedChangeListener {
                    override fun onClick(v: View?) {

                    }

                    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                        val scaleAnimation = ScaleAnimation(0.7f, 1.0f, 0.7f, 1.0f,
                                Animation.RELATIVE_TO_SELF, 0.7f, Animation.RELATIVE_TO_SELF, 0.7f)
                        scaleAnimation.duration = 500
                        val bounceInterpolator = BounceInterpolator()
                        scaleAnimation.interpolator = bounceInterpolator
                        btnFavorite.startAnimation(scaleAnimation)
                        if (isChecked) {
                            favoriteListener.onChecked(root, adapterPosition)
                        } else {
                            favoriteListener.onUnChecked(root, adapterPosition)
                        }
                    }
                })
            }
        }

        fun bind(item: TvResponse.TvShow) {
            with(binding) {
                tvTitle.text = item.name
                if (item.firstAirDate.isNullOrEmpty() || item.firstAirDate == "") {
                    tvDate.text = "-"
                } else {
                    tvDate.text = item.firstAirDate.let { DateTimeUtils.convertToHumanDate(it) }
                }
                val url = "https://image.tmdb.org/t/p/w600_and_h900_bestv2"
                val poster = url + item.posterPath
                if (item.posterPath == null) {
                    Picasso.get().load(R.drawable.no_image).into(ivPoster)
                } else {
                    Picasso.get().load(poster).into(ivPoster)
                }
            }
        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence?): FilterResults {
                val charString = charSequence.toString()
                tvArrayListFiltered = if (charString.isEmpty()) {
                    tvArrayList
                } else {
                    val filteredList = ArrayList<TvResponse.TvShow>()
                    for (row in tvArrayList) {
                        if (row.name!!.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row)
                        }
                    }
                    filteredList
                }
                val filterResult = FilterResults()
                filterResult.values = tvArrayListFiltered
                return filterResult
            }

            override fun publishResults(charSequence: CharSequence?, results: FilterResults) {
                tvArrayListFiltered = results.values as ArrayList<TvResponse.TvShow>
                notifyDataSetChanged()
            }

        }
    }

}
