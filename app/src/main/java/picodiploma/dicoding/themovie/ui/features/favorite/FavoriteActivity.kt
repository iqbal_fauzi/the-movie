package picodiploma.dicoding.themovie.ui.features.favorite

import android.os.Bundle
import kotlinx.android.synthetic.main.activity_favorite.*
import picodiploma.dicoding.themovie.R
import picodiploma.dicoding.themovie.base.BaseActivityVM
import picodiploma.dicoding.themovie.databinding.ActivityFavoriteBinding
import picodiploma.dicoding.themovie.ui.features.adapter.SectionsPagerAdapter
import picodiploma.dicoding.themovie.ui.features.favorite.fragments.FavoriteMovieFragment
import picodiploma.dicoding.themovie.ui.features.favorite.fragments.FavoriteTvFragment
import picodiploma.dicoding.themovie.viewmodel.FavoriteViewModel

class FavoriteActivity : BaseActivityVM<FavoriteViewModel, ActivityFavoriteBinding>(FavoriteViewModel::class) {

    override fun onInitializedView(savedInstanceState: Bundle?) {
        initToolbar()
        setTabAdapter()
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Favorite"
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    override fun getLayout(): Int {
        return R.layout.activity_favorite
    }

    private fun setTabAdapter() {
        with(databinding) {
            val tabAdapter = SectionsPagerAdapter(supportFragmentManager)
            tabAdapter.addFragment(FavoriteMovieFragment(), getString(R.string.movie))
            tabAdapter.addFragment(FavoriteTvFragment(), getString(R.string.tv_show))
            viewPager.adapter = tabAdapter
            tabs.setupWithViewPager(viewPager, true)
        }
    }
}