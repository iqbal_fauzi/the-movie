package picodiploma.dicoding.themovie.ui.features.search

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import picodiploma.dicoding.themovie.R
import picodiploma.dicoding.themovie.base.BaseActivityVM
import picodiploma.dicoding.themovie.data.local.FavoriteMovie
import picodiploma.dicoding.themovie.data.local.FavoriteTv
import picodiploma.dicoding.themovie.databinding.ActivitySearchBinding
import picodiploma.dicoding.themovie.ui.features.adapter.MovieAdapter
import picodiploma.dicoding.themovie.ui.features.adapter.TvAdapter
import picodiploma.dicoding.themovie.ui.features.details.DetailActivity
import picodiploma.dicoding.themovie.ui.listener.OnFavoriteCheckListener
import picodiploma.dicoding.themovie.ui.listener.OnItemClickListener
import picodiploma.dicoding.themovie.util.hide
import picodiploma.dicoding.themovie.util.show
import picodiploma.dicoding.themovie.viewmodel.SearchViewModel

class SearchActivity : BaseActivityVM<SearchViewModel, ActivitySearchBinding>(SearchViewModel::class) {

    override fun onInitializedView(savedInstanceState: Bundle?) {
        with(databinding) {
            setSupportActionBar(toolbar)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
        onRequest()
        setRecycler()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.search_menu, menu)

        val mSearchItem = menu.findItem(R.id.action_search)
        val mSearchView = mSearchItem.actionView as SearchView
        mSearchView.queryHint = "Search"
        mSearchView.isFocusable = true
        mSearchView.isIconified = false

        mSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                if (intent.getStringExtra(TYPE) == "movie") {
                    viewModel.findMovie(query)
                } else viewModel.findTv(query)
                return true
            }

            override fun onQueryTextChange(query: String?): Boolean {
                return true
            }
        })
        return super.onCreateOptionsMenu(menu)
    }

    private fun onRequest() {
        with(databinding) {
            //request now Playing Movie
            viewModel.onRequestData.observe(this@SearchActivity, Observer {
                if (it) {
                    pbLoading.show()
                } else pbLoading.hide()
            })
        }
    }

    private fun setRecycler() {
        with(databinding) {
            if (intent.getStringExtra(TYPE) == "movie") {
                viewModel.movieData.observe(this@SearchActivity, Observer { response ->
                    if (response.isNullOrEmpty()) {
                        rvMovie.hide()
                    } else {
                        with(rvMovie) {
                            show()
                            layoutManager = GridLayoutManager(this@SearchActivity, 2)
                            adapter = MovieAdapter(false, response, object : OnItemClickListener {
                                override fun onItemClick(itemView: View, position: Int) {
                                    val data = Intent(this@SearchActivity, DetailActivity::class.java)
                                    data.putExtra(DetailActivity.ITEM_TYPE, "movie")
                                    data.putExtra(DetailActivity.EXTRA_DATA, response[position])
                                    startActivity(data)
                                }
                            }, object : OnFavoriteCheckListener {
                                override fun onChecked(itemView: View, position: Int) {
                                    val favoriteMovie = FavoriteMovie(
                                        response[position].id.toLong(), response[position].releaseDate,
                                        response[position].title, response[position].posterPath
                                    )
                                    viewModel.setFavoriteMovie(favoriteMovie)
                                }

                                override fun onUnChecked(itemView: View, position: Int) {
                                    val favoriteMovie = FavoriteMovie(
                                        response[position].id.toLong(), response[position].releaseDate,
                                        response[position].title, response[position].posterPath
                                    )
                                    viewModel.removeFavoriteMovie(favoriteMovie)
                                }
                            })
                        }
                    }
                })
            } else {
                viewModel.tvData.observe(this@SearchActivity, Observer { response ->
                    if (response.isNullOrEmpty()) {
                        rvMovie.hide()
                    } else {
                        with(rvMovie) {
                            show()
                            layoutManager = GridLayoutManager(this@SearchActivity, 2)
                            adapter = TvAdapter(false, response, object : OnItemClickListener {
                                override fun onItemClick(itemView: View, position: Int) {
                                    val data = Intent(this@SearchActivity, DetailActivity::class.java)
                                    data.putExtra(DetailActivity.ITEM_TYPE, "tv")
                                    data.putExtra(DetailActivity.EXTRA_DATA, response[position])
                                    startActivity(data)
                                }
                            }, object : OnFavoriteCheckListener {
                                override fun onChecked(itemView: View, position: Int) {
                                    val favoriteTv = FavoriteTv(
                                        response[position].id,
                                        response[position].firstAirDate,
                                        response[position].originalName,
                                        response[position].posterPath
                                    )
                                    viewModel.setFavoriteTvShow(favoriteTv)
                                }

                                override fun onUnChecked(itemView: View, position: Int) {
                                    val favoriteTv = FavoriteTv(
                                        response[position].id,
                                        response[position].firstAirDate,
                                        response[position].originalName,
                                        response[position].posterPath
                                    )
                                    viewModel.removeFavoriteTvShow(favoriteTv)
                                }

                            })
                        }
                    }
                })
            }
        }
    }

    override fun getLayout(): Int {
        return R.layout.activity_search
    }

    companion object {
        const val TYPE = "type"
    }
}
