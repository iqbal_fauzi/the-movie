package picodiploma.dicoding.themovie.ui.features.widget.tv

import android.content.Intent
import android.widget.RemoteViewsService
import picodiploma.dicoding.themovie.ui.features.widget.movie.MovieStackRemoteViewsFactory

/**
 * Created by dicoding on 1/9/2017.
 */

class TvStackWidgetService : RemoteViewsService() {
    override fun onGetViewFactory(intent: Intent): RemoteViewsFactory {
        return TvStackRemoteViewsFactory(this.applicationContext)
    }
}