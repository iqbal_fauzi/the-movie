package picodiploma.dicoding.themovie.ui.features.widget.movie

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.RemoteViews
import android.widget.RemoteViewsService
import com.bumptech.glide.Glide
import picodiploma.dicoding.themovie.R
import picodiploma.dicoding.themovie.data.local.FavoriteMovie
import picodiploma.dicoding.themovie.data.local.MovieDatabase
import picodiploma.dicoding.themovie.data.local.repository.FavoriteMovieRepository
import kotlin.collections.ArrayList

internal class MovieStackRemoteViewsFactory(private val mContext: Context) : RemoteViewsService.RemoteViewsFactory {

    private var list = ArrayList<FavoriteMovie>()
    private var movieDb: MovieDatabase = MovieDatabase.getInstance(mContext)!!
    private var favoriteMovie = FavoriteMovieRepository(movieDb.favoriteMovieDao())

    override fun onCreate() {

    }

    override fun onDataSetChanged() {
        list.addAll(favoriteMovie.getAllMovies())
//        for (i in list.indices) {
//            mWidgetItems.add(list[i].posterPath.toString())
//        }
    }

    override fun onDestroy() {

    }

    override fun getCount(): Int {
        return list.size
    }

    override fun getViewAt(position: Int): RemoteViews {
        val rv = RemoteViews(mContext.packageName, R.layout.widget_item)
        val url = "https://image.tmdb.org/t/p/w600_and_h900_bestv2"

        val bitMap = Glide.with(mContext)
            .asBitmap()
            .load(url + list[position].posterPath)
            .submit(512, 512)
            .get()

        rv.setImageViewBitmap(R.id.imageView, bitMap)

        val extras = Bundle()
        extras.putInt(MovieImageBannerWidget.EXTRA_ITEM, position)
        val fillInIntent = Intent()
        fillInIntent.putExtras(extras)

        rv.setOnClickFillInIntent(R.id.imageView, fillInIntent)
        return rv
    }

    override fun getLoadingView(): RemoteViews? {
        return null
    }

    override fun getViewTypeCount(): Int {
        return 1
    }

    override fun getItemId(i: Int): Long {
        return 0
    }

    override fun hasStableIds(): Boolean {
        return false
    }

}