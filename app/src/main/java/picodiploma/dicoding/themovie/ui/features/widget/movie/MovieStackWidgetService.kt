package picodiploma.dicoding.themovie.ui.features.widget.movie

import android.content.Intent
import android.widget.RemoteViewsService

/**
 * Created by dicoding on 1/9/2017.
 */

class MovieStackWidgetService : RemoteViewsService() {
    override fun onGetViewFactory(intent: Intent): RemoteViewsFactory {
        return MovieStackRemoteViewsFactory(this.applicationContext)
    }
}