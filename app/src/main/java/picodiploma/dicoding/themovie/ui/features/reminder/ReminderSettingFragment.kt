package picodiploma.dicoding.themovie.ui.features.reminder


import android.content.SharedPreferences
import android.os.Bundle
import android.widget.Toast
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreferenceCompat
import picodiploma.dicoding.themovie.R
import picodiploma.dicoding.themovie.service.DailyReleaseReceiver
import picodiploma.dicoding.themovie.service.DailyReminderReceiver

class ReminderSettingFragment : PreferenceFragmentCompat(), SharedPreferences.OnSharedPreferenceChangeListener {

    private var DAILY = ""
    private var RELEASE = ""

    private var dailyPreference: SwitchPreferenceCompat? = null
    private var releasePreferences:SwitchPreferenceCompat? = null
    private lateinit var dailyReminderReceiver: DailyReminderReceiver
    private lateinit var dailyReleaseReceiver: DailyReleaseReceiver

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        if (key == DAILY) {
            val isActive = sharedPreferences?.getBoolean(DAILY, false)
            dailyPreference?.isChecked = isActive!!

            if (isActive) {
                dailyReminderReceiver.setDailyReminder(activity)
                Toast.makeText(activity, resources.getString(R.string.reminder_activated), Toast.LENGTH_SHORT).show()
            } else {
                dailyReminderReceiver.cancelDailyReminder(activity)
                Toast.makeText(activity, resources.getString(R.string.reminder_deactivated), Toast.LENGTH_SHORT).show()
            }
        }
        if (key == RELEASE) {
            val isActive = sharedPreferences?.getBoolean(RELEASE, false)
            releasePreferences?.isChecked = isActive!!

            if (isActive) {
                dailyReleaseReceiver.setDailyReminder(activity)
                Toast.makeText(activity, resources.getString(R.string.reminder_activated), Toast.LENGTH_SHORT).show()
            } else {
                dailyReleaseReceiver.cancelDailyReminder(activity)
                Toast.makeText(activity, resources.getString(R.string.reminder_deactivated), Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences)
        init()
        setSettings()
    }

    private fun setSettings() {
        val sh = preferenceManager.sharedPreferences
        dailyPreference?.isChecked = sh.getBoolean(DAILY, false)
        dailyReminderReceiver = DailyReminderReceiver()
        releasePreferences?.isChecked = sh.getBoolean(RELEASE, false)
        dailyReleaseReceiver = DailyReleaseReceiver()
    }

    private fun init() {
        DAILY = resources.getString(R.string.key_daily)
        RELEASE = resources.getString(R.string.key_release)

        dailyPreference = findPreference(DAILY)
        releasePreferences = findPreference(RELEASE)
    }

    override fun onResume() {
        super.onResume()
        preferenceScreen.sharedPreferences.registerOnSharedPreferenceChangeListener(this)
    }

    override fun onPause() {
        super.onPause()
        preferenceScreen.sharedPreferences.unregisterOnSharedPreferenceChangeListener(this)
    }
}
