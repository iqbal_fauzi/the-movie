package picodiploma.dicoding.themovie.ui.features.favorite.fragments


import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import picodiploma.dicoding.themovie.R
import picodiploma.dicoding.themovie.base.BaseFragmentVM
import picodiploma.dicoding.themovie.data.local.FavoriteTv
import picodiploma.dicoding.themovie.databinding.FragmentFavoriteBinding
import picodiploma.dicoding.themovie.ui.features.adapter.FavoriteTvAdapter
import picodiploma.dicoding.themovie.ui.features.details.DetailActivity
import picodiploma.dicoding.themovie.ui.features.favorite.FavoriteActivity
import picodiploma.dicoding.themovie.ui.listener.OnFavoriteCheckListener
import picodiploma.dicoding.themovie.ui.listener.OnItemClickListener
import picodiploma.dicoding.themovie.util.hide
import picodiploma.dicoding.themovie.util.show
import picodiploma.dicoding.themovie.util.toastShort
import picodiploma.dicoding.themovie.viewmodel.FavoriteViewModel

class FavoriteTvFragment : BaseFragmentVM<FavoriteViewModel, FragmentFavoriteBinding>(FavoriteViewModel::class) {

    private lateinit var tvAdapter: FavoriteTvAdapter
    private lateinit var arrayList: ArrayList<FavoriteTv>

    override fun onViewInitialized(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        viewModel.getAllFavoritTv()
        setRecycler()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.options_menu, menu)
        val favorite = menu.findItem(R.id.favorite_menu)
        val setting = menu.findItem(R.id.action_change_settings)
        setting.isVisible = false
        favorite.isVisible = false
        val mSearch = menu.findItem(R.id.action_search)
        val mSearchView = mSearch.actionView as SearchView
        mSearchView.queryHint = "Search Tv"
        mSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                tvAdapter.filter.filter(query)
                return true
            }

            override fun onQueryTextChange(query: String?): Boolean {
                tvAdapter.filter.filter(query)
                return true
            }
        })
        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.favorite_menu -> {
                startActivity(Intent(activity, FavoriteActivity::class.java))
                true
            }
            R.id.action_change_settings -> {
                val mIntent = Intent(Settings.ACTION_LOCALE_SETTINGS)
                startActivity(mIntent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setRecycler() {
        //get Favorite Movie Data
        viewModel.favoriteTvData.observe(this, Observer { values ->
            with(databinding) {
                rvFavorite.apply {
                    if (values.isNotEmpty()) {
                        show()
                        arrayList = values as ArrayList<FavoriteTv>
                        layoutManager = GridLayoutManager(mContext, 2)
                        tvAdapter = FavoriteTvAdapter(false, arrayList, object : OnItemClickListener {
                            override fun onItemClick(itemView: View, position: Int) {
                                val data = Intent(mContext, DetailActivity::class.java)
                                data.putExtra(DetailActivity.ITEM_TYPE, "favorite_tv")
                                data.putExtra(DetailActivity.EXTRA_DATA, values[position])
                                startActivity(data)
                            }
                        }, object : OnFavoriteCheckListener {
                            override fun onChecked(itemView: View, position: Int) {
                                //empty function
                            }

                            override fun onUnChecked(itemView: View, position: Int) {
                                viewModel.removeFavoriteTvShow(values[position])
                                mContext?.toastShort("Data deleted from favorite")
                            }
                        })
                        adapter = tvAdapter
                    } else {
                        tvEmpty.show()
                        hide()
                    }
                }
            }
        })
    }

    override fun getResourceLayout(): Int {
        return R.layout.fragment_favorite
    }
}
