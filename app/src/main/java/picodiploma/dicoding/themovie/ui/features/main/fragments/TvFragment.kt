package picodiploma.dicoding.themovie.ui.features.main.fragments


import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import picodiploma.dicoding.themovie.R
import picodiploma.dicoding.themovie.base.BaseFragmentVM
import picodiploma.dicoding.themovie.data.local.FavoriteTv
import picodiploma.dicoding.themovie.data.response.TvResponse
import picodiploma.dicoding.themovie.databinding.FragmentMovieBinding
import picodiploma.dicoding.themovie.ui.features.adapter.TvAdapter
import picodiploma.dicoding.themovie.ui.features.details.DetailActivity
import picodiploma.dicoding.themovie.ui.features.favorite.FavoriteActivity
import picodiploma.dicoding.themovie.ui.features.reminder.ReminderSettingActivity
import picodiploma.dicoding.themovie.ui.features.search.SearchActivity
import picodiploma.dicoding.themovie.ui.listener.OnFavoriteCheckListener
import picodiploma.dicoding.themovie.ui.listener.OnItemClickListener
import picodiploma.dicoding.themovie.util.hide
import picodiploma.dicoding.themovie.util.show
import picodiploma.dicoding.themovie.util.snackBar
import picodiploma.dicoding.themovie.util.toastShort
import picodiploma.dicoding.themovie.viewmodel.TvViewModel

class TvFragment : BaseFragmentVM<TvViewModel, FragmentMovieBinding>(TvViewModel::class) {

    private lateinit var tvAdapter: TvAdapter
    private lateinit var arrayList: ArrayList<TvResponse.TvShow>

    override fun onViewInitialized(savedInstanceState: Bundle?) {
        onRequest()
        setHasOptionsMenu(true)
        with(viewModel) {
            getAiringToday()
        }
        setRecycler()
        showErrorMessage()
    }

    private fun showErrorMessage() {
        with(viewModel) {
            message.observe(this@TvFragment, Observer {
                databinding.root.snackBar(it)
            })

            handleError.observe(this@TvFragment, Observer {
                handleError(it)
            })
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_search -> {
                val searchIntent = Intent(activity, SearchActivity::class.java)
                searchIntent.putExtra(SearchActivity.TYPE, "tv")
                startActivity(searchIntent)
                true
            }
            R.id.favorite_menu -> {
                startActivity(Intent(activity, FavoriteActivity::class.java))
                true
            }
            R.id.action_change_settings -> {
                val mIntent = Intent(Settings.ACTION_LOCALE_SETTINGS)
                startActivity(mIntent)
                true
            }
            R.id.action_reminder_settings -> {
                startActivity(Intent(activity, ReminderSettingActivity::class.java))
                true
            }
            else -> true
        }
    }

    private fun setRecycler() {
        viewModel.tvData.observe(this, Observer { response ->
            with(databinding) {
                with(rv1) {
                    if (response.isNullOrEmpty()) {
                        hide()
                        tvEmpty.show()
                    } else {
                        show()
                        tvEmpty.hide()
                        arrayList = response as ArrayList<TvResponse.TvShow>
                        layoutManager = GridLayoutManager(mContext, 2)
                        tvAdapter = TvAdapter(false, response, object : OnItemClickListener {
                            override fun onItemClick(itemView: View, position: Int) {
                                val data = Intent(mContext, DetailActivity::class.java)
                                data.putExtra(DetailActivity.ITEM_TYPE, "tv")
                                data.putExtra(DetailActivity.EXTRA_DATA, response[position])
                                startActivity(data)
                            }
                        }, object : OnFavoriteCheckListener {
                            override fun onChecked(itemView: View, position: Int) {
                                val favoriteTv = FavoriteTv(response[position].id, response[position].firstAirDate,
                                        response[position].originalName, response[position].posterPath)
                                viewModel.setFavoriteTvShow(favoriteTv)
                                mContext?.toastShort("Data added to favorite")
                            }

                            override fun onUnChecked(itemView: View, position: Int) {
                                val favoriteTv = FavoriteTv(response[position].id, response[position].firstAirDate,
                                        response[position].originalName, response[position].posterPath)
                                viewModel.removeFavoriteTvShow(favoriteTv)
                                mContext?.toastShort("Data deleted from favorite")
                            }
                        })
                        adapter = tvAdapter
                    }
                }
            }
        })
    }

    private fun onRequest() {
        with(databinding) {
            //request now Playing Movie
            viewModel.onRequestData.observe(this@TvFragment, Observer {
                if (it) {
                    pbLoading.show()
                } else pbLoading.hide()
            })
        }
    }

    override fun getResourceLayout(): Int {
        return R.layout.fragment_movie
    }
}
