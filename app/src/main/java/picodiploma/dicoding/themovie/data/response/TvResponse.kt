package picodiploma.dicoding.themovie.data.response

import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.StringRes
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parceler
import kotlinx.android.parcel.Parcelize
import picodiploma.dicoding.themovie.base.BaseResponse

/**
 * Created by Iqbal Fauzi on 14:52 13/07/19
 */
class TvResponse : BaseResponse() {
    @SerializedName("page")
    var page: Int? = null
    @SerializedName("results")
    var results: List<TvShow>? = null
    @SerializedName("total_pages")
    var totalPages: Int? = null
    @SerializedName("total_results")
    var totalResults: Int? = null

    @Parcelize
    @Entity(tableName = "tvshow")
    data class TvShow(
        @SerializedName("poster_path")
        @ColumnInfo(name = "poster_path")
        val posterPath: String? = null,

        @SerializedName("popularity")
        @ColumnInfo(name = "popularity")
        val popularity: Double? = null,

        @SerializedName("id")
        @PrimaryKey
        @ColumnInfo(name = "id")
        val id: Int? = null,

        @SerializedName("backdrop_path")
        @ColumnInfo(name = "backdrop_path")
        val backdropPath: String? = null,

        @SerializedName("vote_average")
        @ColumnInfo(name = "vote_average")
        val voteAverage: Double? = null,

        @SerializedName("overview")
        @ColumnInfo(name = "overview")
        val overview: String? = null,

        @SerializedName("first_air_date")
        @ColumnInfo(name = "first_air_date")
        val firstAirDate: String? = null,

        @SerializedName("origin_country")
        @ColumnInfo(name = "origin_country")
        val originCountry: List<String>? = null,

        @SerializedName("genre_ids")
        @ColumnInfo(name = "genre_ids")
        val genreIds: List<Int>? = null,

        @SerializedName("original_language")
        @ColumnInfo(name = "original_language")
        val originalLanguage: String? = null,

        @SerializedName("vote_count")
        @ColumnInfo(name = "vote_count")
        val voteCount: Int? = null,

        @SerializedName("name")
        @ColumnInfo(name = "name")
        val name: String? = null,

        @SerializedName("original_name")
        @ColumnInfo(name = "original_name")
        val originalName: String? = null

    ) : Parcelable
}