package picodiploma.dicoding.themovie.data.local.dao

import android.database.Cursor
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import picodiploma.dicoding.themovie.data.local.FavoriteMovie

/**
 * Created by Iqbal Fauzi on 14:58 10/08/19
 */
@Dao
interface FavoriteMovieDao {

    @Query("SELECT * from favorite_movie")
    fun getAll(): List<FavoriteMovie>

    @Insert(onConflict = REPLACE)
    fun insert(movie: FavoriteMovie)

    @Delete
    fun delete(movie: FavoriteMovie)

    @Query("DELETE FROM movies")
    fun deleteAll()

    @Query("SELECT * FROM favorite_movie")
    fun selectAll(): Cursor

    @Query("SELECT * FROM favorite_movie WHERE id=:id")
    fun selectById(id: Long): Cursor

    @Query("DELETE FROM favorite_movie WHERE id=:id")
    fun deleteById(id: Long): Int

    @Insert(onConflict = REPLACE)
    fun inserts(movies: Array<FavoriteMovie?>) : LongArray

    @Insert
    fun insertMovie(movie: FavoriteMovie) : Long
}