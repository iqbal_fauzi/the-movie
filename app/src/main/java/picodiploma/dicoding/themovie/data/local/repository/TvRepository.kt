package picodiploma.dicoding.themovie.data.local.repository

import picodiploma.dicoding.themovie.data.local.dao.TvDao
import picodiploma.dicoding.themovie.data.response.TvResponse

/**
 * Created by Iqbal Fauzi on 14:34 13/08/19
 */
class TvRepository(private val tvDao: TvDao) {

    fun getAllTvShows(): List<TvResponse.TvShow> {
        return tvDao.getAll()
    }

    fun saveListTvShow(tvShow: TvResponse) {
        tvDao.inserts(tvShow.results)
    }

    fun saveTvShow(tvShow: TvResponse.TvShow) {
        tvDao.insert(tvShow)
    }

    fun deleteTvShow(tvShow: TvResponse.TvShow) {
        tvDao.delete(tvShow.id)
    }

    fun clearAll() {
        tvDao.deleteAll()
    }

}