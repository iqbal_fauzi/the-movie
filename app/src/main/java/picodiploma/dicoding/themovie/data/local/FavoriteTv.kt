package picodiploma.dicoding.themovie.data.local

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

/**
 * Created by Iqbal Fauzi on 10:02 14/08/19
 */
@Parcelize
@Entity(tableName = "favorite_tv")
data class FavoriteTv(

        @PrimaryKey
        @ColumnInfo(name = "id")
        val id: Int?,

        @ColumnInfo(name = "release_date")
        val releaseDate: String?,

        @ColumnInfo(name = "title")
        val title: String?,

        @ColumnInfo(name = "poster_path")
        val posterPath: String?

) : Parcelable