package picodiploma.dicoding.themovie.data.network

import io.reactivex.Observable
import picodiploma.dicoding.themovie.data.response.MovieDetailResponse
import picodiploma.dicoding.themovie.data.response.MovieResponse
import picodiploma.dicoding.themovie.data.response.TvDetailResponse
import picodiploma.dicoding.themovie.data.response.TvResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by Iqbal Fauzi on 19:37 13/07/19
 */
interface ApiService {

    @GET("movie/now_playing/")
    fun getNowPlayingMovie(): Observable<MovieResponse>

    @GET("movie/upcoming/")
    fun getUpcomingMovie(): Observable<MovieResponse>

    @GET("movie/{movie_id}")
    fun getDetailMovie(@Path("movie_id") movieId: String): Call<MovieDetailResponse>

    @GET("tv/airing_today/")
    fun getAiringTv(): Observable<TvResponse>

    @GET("tv/popular/")
    fun getPopularTv(): Observable<TvResponse>

    @GET("tv/{tv_id}")
    fun getDetailTv(@Path("tv_id") tvId: String): Call<TvDetailResponse>

    @GET("search/movie?")
    fun findMovies(@Query("query") movieName: String): Observable<MovieResponse>

    @GET("search/tv?")
    fun findTv(@Query("query") tvName: String): Observable<TvResponse>

    @GET("/3/{type}/{category}")
    fun getMovieRelease(@Path("type") type: String,
                        @Path("category") category: String,
                        @Query("api_key") apiKey: String,
                        @Query("primary_release_date.gte") releaseDate: String,
                        @Query("primary_release_date.lte") reReleaseDate: String
    ): Observable<MovieResponse>
}