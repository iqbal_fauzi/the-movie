package picodiploma.dicoding.themovie.data.response

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import picodiploma.dicoding.themovie.base.BaseResponse

/**
 * Created by Iqbal Fauzi on 14:52 13/07/19
 */
class MovieResponse : BaseResponse(){
    @SerializedName("page")
    var page: Int? = null
    @SerializedName("results")
    var results: List<Movies>? = null
    @SerializedName("dates")
    var dates: Dates? = null
    @SerializedName("total_pages")
    var totalPages: Int? = null
    @SerializedName("total_results")
    var totalResults: Int? = null

    @Parcelize
    @Entity(tableName = "movies")
    data class Movies(
        @SerializedName("poster_path")
        @ColumnInfo(name = "poster_path")
        val posterPath: String,

        @SerializedName("adult")
        @ColumnInfo(name = "adult")
        val adult: Boolean,

        @SerializedName("overview")
        @ColumnInfo(name = "overview")
        val overView: String,

        @SerializedName("release_date")
        @ColumnInfo(name = "release_date")
        val releaseDate: String,

        @SerializedName("genre_ids")
        @ColumnInfo(name = "genre_ids")
        val genreIds: List<Int>,

        @PrimaryKey
        @SerializedName("id")
        @ColumnInfo(name = "id")
        val id: Int,

        @SerializedName("original_title")
        @ColumnInfo(name = "original_title")
        val originalTitle: String,

        @SerializedName("original_language")
        @ColumnInfo(name = "original_language")
        val originalLanguage: String,

        @SerializedName("title")
        @ColumnInfo(name = "title")
        val title: String,

        @SerializedName("backdrop_path")
        @ColumnInfo(name = "backdrop_path")
        val backdropPath: String,

        @SerializedName("popularity")
        @ColumnInfo(name = "popularity")
        val popularity: Double,

        @SerializedName("vote_count")
        @ColumnInfo(name = "vote_count")
        val voteCount: Int,

        @SerializedName("video")
        @ColumnInfo(name = "vide")
        val video: Boolean,

        @SerializedName("vote_average")
        @ColumnInfo(name = "vote_average")
        val vote_average: Double

    ) : Parcelable

    class Dates {
        @SerializedName("maximum")
        val maximum: String? = null
        @SerializedName("minimum")
        val minimum: String? = null
    }
}