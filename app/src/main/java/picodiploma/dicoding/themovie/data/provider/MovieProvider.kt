package picodiploma.dicoding.themovie.data.provider

import android.content.ContentProvider
import android.content.ContentUris
import android.content.ContentValues
import android.content.UriMatcher
import android.database.Cursor
import android.net.Uri
import picodiploma.dicoding.themovie.data.local.FavoriteMovie
import picodiploma.dicoding.themovie.data.local.MovieDatabase
import picodiploma.dicoding.themovie.data.local.dao.FavoriteMovieDao

/**
 * Created by Iqbal Fauzi on 15:03 20/09/19
 */
class MovieProvider : ContentProvider() {

    private lateinit var resultDatabase: MovieDatabase
    private lateinit var favoriteMovieDao: FavoriteMovieDao

    override fun onCreate(): Boolean {
        resultDatabase = MovieDatabase.getInstance(context)!!
        favoriteMovieDao = resultDatabase.favoriteMovieDao()
        return true
    }

    override fun query(uri: Uri, projection: Array<out String>?, selection: String?,
                       selectionArgs: Array<out String>?, sortOrder: String?): Cursor? {
        val code = MATCHER.match(uri)
        if (code == CODE_MOVIE_DIR || code == CODE_MOVIE_ITEM) {
            val mContext = context ?: return null
            val cursor: Cursor = if (code == CODE_MOVIE_DIR) {
                favoriteMovieDao.selectAll()
            } else {
                favoriteMovieDao.selectById(ContentUris.parseId(uri))
            }
            cursor.setNotificationUri(mContext.contentResolver, uri)
            return cursor
        } else {
            throw IllegalArgumentException("Unknown URI: $uri")
        }
    }

    override fun getType(uri: Uri): String? {
        return when (MATCHER.match(uri)) {
            CODE_MOVIE_DIR -> "vnd.android.cursor.dir/$AUTHORITY.favorite_movie"
            CODE_MOVIE_ITEM -> "vnd.android.cursor.dir/$AUTHORITY.favorite_movie"
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        when (MATCHER.match(uri)) {
            CODE_MOVIE_DIR -> {
                val mContext = context ?: return null
                assert(values != null)
                val id: Long = favoriteMovieDao.insertMovie(FavoriteMovie.fromContentValues(values!!))
                mContext.contentResolver.notifyChange(uri, null)
                return ContentUris.withAppendedId(uri, id)
            }
            CODE_MOVIE_ITEM -> throw IllegalArgumentException("Invalid URI, cannot insert with ID: $uri")
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<out String>?): Int {
        when (MATCHER.match(uri)) {
            CODE_MOVIE_DIR -> {
                throw IllegalArgumentException("Invalid URI, cannot update without ID$uri")
            }
            CODE_MOVIE_ITEM -> {
                val context = context ?: return 0
                val count = favoriteMovieDao.deleteById(ContentUris.parseId(uri))
                context.contentResolver.notifyChange(uri, null)
                return count
            }
            else -> throw IllegalArgumentException("Uknown URI: $uri")
        }
    }

    override fun update(p0: Uri, p1: ContentValues?, p2: String?, p3: Array<out String>?): Int {
        return 0
    }

    override fun bulkInsert(uri: Uri, valuesArray: Array<ContentValues>): Int {
        when (MATCHER.match(uri)) {
            CODE_MOVIE_DIR -> {
                context ?: return 0
                val movieResults = arrayOfNulls<FavoriteMovie>(valuesArray.size)
                for (i in valuesArray.indices) {
                    movieResults[i] = FavoriteMovie.fromContentValues(valuesArray[i])
                }
                return resultDatabase.favoriteMovieDao().inserts(movieResults).size
            }
            CODE_MOVIE_ITEM -> throw IllegalArgumentException("Invalid URI, cannot insert with ID: $uri")
            else -> throw IllegalArgumentException("Uknown URI: $uri")
        }
    }

    companion object {
        const val AUTHORITY = "picodiploma.dicoding.themovie.data.provider.MovieProvider"
        val URI_MOVIE = Uri.parse("content://$AUTHORITY/favorite_movie")!!

        const val CODE_MOVIE_DIR = 1
        const val CODE_MOVIE_ITEM = 2
        val MATCHER = UriMatcher(UriMatcher.NO_MATCH)

        init {
            MATCHER.addURI(AUTHORITY, "favorite_movie", CODE_MOVIE_DIR)
            MATCHER.addURI(AUTHORITY, "favorite_movie/*", CODE_MOVIE_ITEM)
        }
    }

}