package picodiploma.dicoding.themovie.data.network

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Iqbal Fauzi on 9:58 01/07/19
 */
class ApiError {
    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("errors")
    @Expose
    var errors: List<Error>? = null
    @SerializedName("statusCode")
    @Expose
    var statusCode: String? = null
}