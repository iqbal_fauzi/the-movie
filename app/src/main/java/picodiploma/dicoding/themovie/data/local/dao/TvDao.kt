package picodiploma.dicoding.themovie.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import picodiploma.dicoding.themovie.data.response.TvResponse

/**
 * Created by Iqbal Fauzi on 14:58 10/08/19
 */
@Dao
interface TvDao {

    @Query("SELECT * from tvshow")
    fun getAll(): List<TvResponse.TvShow>

    @Insert(onConflict = REPLACE)
    fun insert(tvshow: TvResponse.TvShow)

    @Insert(onConflict = REPLACE)
    fun inserts(tvshows: List<TvResponse.TvShow>?)

    @Query("DELETE FROM tvshow WHERE id=:tvId")
    fun delete(tvId: Int?)

    @Query("DELETE FROM tvshow")
    fun deleteAll()


}