package picodiploma.dicoding.themovie.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import picodiploma.dicoding.themovie.data.local.converter.MovieGenreConverter
import picodiploma.dicoding.themovie.data.local.converter.TvCountryConverter
import picodiploma.dicoding.themovie.data.local.dao.FavoriteMovieDao
import picodiploma.dicoding.themovie.data.local.dao.FavoriteTvDao
import picodiploma.dicoding.themovie.data.local.dao.MovieDao
import picodiploma.dicoding.themovie.data.local.dao.TvDao
import picodiploma.dicoding.themovie.data.response.MovieResponse
import picodiploma.dicoding.themovie.data.response.TvResponse

/**
 * Created by Iqbal Fauzi on 15:07 10/08/19
 */
@Database(
        entities = [MovieResponse.Movies::class, FavoriteMovie::class, TvResponse.TvShow::class, FavoriteTv::class],
        version = 2,
        exportSchema = false)
@TypeConverters(MovieGenreConverter::class, TvCountryConverter::class)
abstract class MovieDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDao
    abstract fun tvDao(): TvDao
    abstract fun favoriteMovieDao(): FavoriteMovieDao
    abstract fun favoriteTvDao(): FavoriteTvDao

    //Traditional method to get instance
    companion object {
        private var INSTANCE: MovieDatabase? = null

        fun getInstance(context: Context?): MovieDatabase? {
            if (INSTANCE == null) {
                synchronized(MovieDatabase::class) {
                    INSTANCE = context?.applicationContext?.let {
                        Room.databaseBuilder(it, MovieDatabase::class.java, "movieDatabase.db").build()
                    }
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}