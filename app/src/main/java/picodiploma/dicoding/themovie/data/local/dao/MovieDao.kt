package picodiploma.dicoding.themovie.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import picodiploma.dicoding.themovie.data.response.MovieResponse

/**
 * Created by Iqbal Fauzi on 14:58 10/08/19
 */
@Dao
interface MovieDao {

    @Query("SELECT * from movies")
    fun getAll(): List<MovieResponse.Movies>

    @Insert(onConflict = REPLACE)
    fun insert(movie: MovieResponse.Movies)

    @Insert(onConflict = REPLACE)
    fun inserts(movie: List<MovieResponse.Movies>?)

    @Query("DELETE FROM movies WHERE id=:movieId")
    fun delete(movieId: Int)

    @Query("DELETE FROM movies")
    fun deleteAll()

}