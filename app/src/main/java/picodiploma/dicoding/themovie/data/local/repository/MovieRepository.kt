package picodiploma.dicoding.themovie.data.local.repository

import android.app.Application
import picodiploma.dicoding.themovie.data.local.dao.MovieDao
import picodiploma.dicoding.themovie.data.response.MovieResponse

/**
 * Created by Iqbal Fauzi on 9:20 13/08/19
 */
class MovieRepository(application: Application) {

    private lateinit var movieDao: MovieDao

    fun getAllMovies() : List<MovieResponse.Movies> {
        return movieDao.getAll()
    }

    fun saveListMovie(movies: MovieResponse) {
        movieDao.inserts(movies.results)
    }

    fun saveMovie(movie: MovieResponse.Movies) {
        movieDao.insert(movie)
    }

    fun deleteMovie(movie: MovieResponse.Movies) {
        movieDao.delete(movie.id)
    }

    fun clearAll() {
        movieDao.deleteAll()
    }

}