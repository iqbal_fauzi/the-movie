package picodiploma.dicoding.themovie.data.network

/**
 * Created by Iqbal Fauzi on 9:56 29/07/19
 */
object ApiConstant {
    const val API_KEY = "api_key"
}