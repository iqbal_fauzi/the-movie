package picodiploma.dicoding.themovie.data.local.repository

import picodiploma.dicoding.themovie.data.local.FavoriteMovie
import picodiploma.dicoding.themovie.data.local.dao.FavoriteMovieDao

/**
 * Created by Iqbal Fauzi on 9:20 13/08/19
 */
class FavoriteMovieRepository(private val favoriteMovieDao: FavoriteMovieDao) {

    fun getAllMovies() : List<FavoriteMovie> {
        return favoriteMovieDao.getAll()
    }

    fun saveMovie(favoriteMovie: FavoriteMovie) {
        favoriteMovieDao.insert(favoriteMovie)
    }

    fun deleteMovie(movie: FavoriteMovie) {
        favoriteMovieDao.delete(movie)
    }

    fun clearAll() {
        favoriteMovieDao.deleteAll()
    }

}