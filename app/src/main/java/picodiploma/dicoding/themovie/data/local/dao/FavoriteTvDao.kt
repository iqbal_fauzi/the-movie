package picodiploma.dicoding.themovie.data.local.dao

import android.database.Cursor
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import picodiploma.dicoding.themovie.data.local.FavoriteMovie
import picodiploma.dicoding.themovie.data.local.FavoriteTv

/**
 * Created by Iqbal Fauzi on 14:58 10/08/19
 */
@Dao
interface FavoriteTvDao {

    @Query("SELECT * from favorite_tv")
    fun getAll(): List<FavoriteTv>

    @Insert(onConflict = REPLACE)
    fun insert(tv: FavoriteTv)

    @Delete
    fun delete(tv: FavoriteTv)

    @Query("DELETE FROM favorite_tv")
    fun deleteAll()

    @Query("SELECT * FROM favorite_tv")
    fun selectAll(): Cursor

    @Query("SELECT * FROM favorite_tv WHERE id=:id")
    fun selectById(id: Long): Cursor

    @Query("DELETE FROM favorite_tv WHERE id=:id")
    fun deleteById(id: Long): Int

    @Insert(onConflict = REPLACE)
    fun inserts(tvs: Array<FavoriteTv?>) : LongArray

    @Insert
    fun insertTv(tvs: FavoriteTv) : Long

}