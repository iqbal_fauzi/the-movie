package picodiploma.dicoding.themovie.data.network

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Iqbal Fauzi on 9:59 01/07/19
 */
class Error {
    @SerializedName("error")
    @Expose
    var error: String? = null
}
