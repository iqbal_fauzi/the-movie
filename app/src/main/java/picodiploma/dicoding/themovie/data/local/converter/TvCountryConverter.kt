package picodiploma.dicoding.themovie.data.local.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

/**
 * Created by Iqbal Fauzi on 15:17 10/08/19
 */
class TvCountryConverter {
    @TypeConverter
    fun fromOriginCountry(originCountry: String): List<String> {
        val listType = object : TypeToken<List<String>>() {}.type
        return Gson().fromJson(originCountry, listType)
    }

    @TypeConverter
    fun toOriginCountry(value: List<String>): String {
        return Gson().toJson(value)
    }
}