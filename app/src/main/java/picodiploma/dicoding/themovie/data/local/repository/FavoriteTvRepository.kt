package picodiploma.dicoding.themovie.data.local.repository

import io.reactivex.disposables.CompositeDisposable
import picodiploma.dicoding.themovie.data.local.FavoriteMovie
import picodiploma.dicoding.themovie.data.local.FavoriteTv
import picodiploma.dicoding.themovie.data.local.dao.FavoriteMovieDao
import picodiploma.dicoding.themovie.data.local.dao.FavoriteTvDao
import picodiploma.dicoding.themovie.data.local.dao.MovieDao
import picodiploma.dicoding.themovie.data.response.MovieResponse

/**
 * Created by Iqbal Fauzi on 9:20 13/08/19
 */
class FavoriteTvRepository(private val favoriteTv: FavoriteTvDao) {

    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    fun getAllTvShow() : List<FavoriteTv> {
        return favoriteTv.getAll()
    }

    fun saveTv(tv: FavoriteTv) {
        favoriteTv.insert(tv)
    }

    fun deleteTv(tv: FavoriteTv) {
        favoriteTv.delete(tv)
    }

    fun clearAll() {
        favoriteTv.deleteAll()
    }

}