package picodiploma.dicoding.themovie.data.local.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

/**
 * Created by Iqbal Fauzi on 15:17 10/08/19
 */
class MovieGenreConverter {
    @TypeConverter
    fun fromGenreIds(genreIds: String): List<Int> {
        val listType = object : TypeToken<List<Int>>() {}.type
        return Gson().fromJson(genreIds, listType)
    }

    @TypeConverter
    fun toGenreIds(value: List<Int>): String {
        return Gson().toJson(value)
    }
}