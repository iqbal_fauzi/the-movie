package picodiploma.dicoding.themovie.data.network

import io.reactivex.functions.Consumer
import picodiploma.dicoding.themovie.base.BaseResponse

/**
 * Created by Iqbal Fauzi on 7:56 24/06/19
 */
abstract class ResponseHandler<M : BaseResponse>(private val successCode: Int?) : Consumer<M> {

    abstract fun onSuccess(model : M)

    abstract fun onUnauthorized()

    abstract fun onError(model: M)

    override fun accept(model : M) {
        when {
            model.code == successCode -> onSuccess(model)
            model.code == 401 -> onUnauthorized()
            else -> onError(model)
        }
    }
}