package picodiploma.dicoding.themovie.data.local

import android.content.ContentValues
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

/**
 * Created by Iqbal Fauzi on 10:02 14/08/19
 */
@Parcelize
@Entity(tableName = "favorite_movie")
data class FavoriteMovie(

    @PrimaryKey
    @ColumnInfo(name = "id")
    var id: Long?,

    @ColumnInfo(name = "release_date")
    var releaseDate: String?,

    @ColumnInfo(name = "title")
    var title: String?,

    @ColumnInfo(name = "poster_path")
    var posterPath: String?

) : Parcelable {

    constructor() : this(null, null, null, null)

    companion object {
        fun fromContentValues(values: ContentValues?) : FavoriteMovie {
            val movieResult = FavoriteMovie()
            if (values?.containsKey("id")!!) {
                movieResult.id = values.getAsLong("id")
            }
            if (values.containsKey("release_date")) {
                movieResult.releaseDate = values.get("release_date").toString()
            }
            if (values.containsKey("title")) {
                movieResult.title = values.getAsString("title")
            }
            if (values.containsKey("poster_path")) {
                movieResult.posterPath = values.getAsString("poster_path")
            }
            return movieResult
        }
    }

}