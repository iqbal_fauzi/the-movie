package picodiploma.dicoding.themovie.base

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import picodiploma.dicoding.themovie.R
import picodiploma.dicoding.themovie.util.snackBarRed
import kotlin.reflect.KClass

/**
 * Created by Iqbal Fauzi on 10:38 31/07/19
 */
abstract class BaseActivityVM<out V : ViewModel, D : ViewDataBinding>(c: KClass<V>): AppCompatActivity() {

    val viewModel: V by viewModel(c)

    var mContext: Context? = null
    lateinit var databinding: D
    var dataReceived: Bundle? = null
    var bundle: Bundle? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        databinding = DataBindingUtil.setContentView(this, getLayout())
        dataReceived = intent.extras
        onInitializedView(savedInstanceState)
    }

    fun handleError(case: Int) {
        when (case) {
            1 -> {
                databinding.root.snackBarRed(getString(R.string.service_not_found))
            }
            2 -> {
                databinding.root.snackBarRed(getString(R.string.network_not_stable))
            }
            3 -> {
                databinding.root.snackBarRed(getString(R.string.server_error))
            }
            4 -> {
                databinding.root.snackBarRed(getString(R.string.service_not_connected))
            }
            5 -> {
                databinding.root.snackBarRed(getString(R.string.some_error))
            }
        }
    }

    abstract fun onInitializedView(savedInstanceState: Bundle?)

    abstract fun getLayout(): Int

}