package picodiploma.dicoding.themovie.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import picodiploma.dicoding.themovie.R
import picodiploma.dicoding.themovie.util.snackBarRed
import kotlin.reflect.KClass

/**
 * Created by Iqbal Fauzi on 20:36 13/07/19
 */
abstract class BaseFragmentVM<out V : ViewModel, D : ViewDataBinding>(c: KClass<V>) : Fragment() {

    val viewModel: V by viewModel(c)

    var mContext: Context? = null
    lateinit var databinding: D
    var dataReceived: Bundle? = null
    var bundle: Bundle? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        databinding = DataBindingUtil.inflate(inflater, getResourceLayout(), container, false)
        dataReceived = arguments
        mContext = activity
        onViewInitialized(savedInstanceState)

        return databinding.root
    }

    fun handleError(case: Int) {
        when (case) {
            1 -> {
                databinding.root.snackBarRed(getString(R.string.service_not_found))
            }
            2 -> {
                databinding.root.snackBarRed(getString(R.string.network_not_stable))
            }
            3 -> {
                databinding.root.snackBarRed(getString(R.string.server_error))
            }
            4 -> {
                databinding.root.snackBarRed(getString(R.string.service_not_connected))
            }
            5 -> {
                databinding.root.snackBarRed(getString(R.string.some_error))
            }
        }
    }

    abstract fun onViewInitialized(savedInstanceState: Bundle?)

    abstract fun getResourceLayout(): Int
}