package picodiploma.dicoding.themovie.base

import com.google.gson.annotations.SerializedName

/**
 * Created by Iqbal Fauzi on 14:54 13/07/19
 */
open class BaseResponse {
    @SerializedName("code")
    var code: Int? = null
    @SerializedName("message")
    var message: String? = null
    @SerializedName("status")
    var status: String? = null
}