package picodiploma.dicoding.themovie.base

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import picodiploma.dicoding.themovie.di.localDataModule
import picodiploma.dicoding.themovie.di.networkModule
import picodiploma.dicoding.themovie.di.viewModelModule

/**
 * Created by Iqbal Fauzi on 19:47 13/07/19
 */
class MovieApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@MovieApp)
            modules(listOf(networkModule, viewModelModule, localDataModule))
        }
    }
}