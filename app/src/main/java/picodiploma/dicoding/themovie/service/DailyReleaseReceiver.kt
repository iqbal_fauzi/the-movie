package picodiploma.dicoding.themovie.service

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.os.Build
import android.os.StrictMode
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.androidnetworking.error.ANError
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import picodiploma.dicoding.themovie.BuildConfig
import picodiploma.dicoding.themovie.R
import picodiploma.dicoding.themovie.data.network.NetworkConfig
import picodiploma.dicoding.themovie.data.network.ResponseHandler
import picodiploma.dicoding.themovie.data.response.MovieResponse
import picodiploma.dicoding.themovie.ui.features.main.MainActivity
import picodiploma.dicoding.themovie.util.DateTimeUtils
import timber.log.Timber
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class DailyReleaseReceiver : BroadcastReceiver() {

    private val DAILY_RELEASE_ID = 102
    private var movieResults: List<MovieResponse.Movies> = ArrayList()
    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onReceive(context: Context, intent: Intent) {
        getMovieReleaseToday(context)
    }

    private fun getMovieReleaseToday(context: Context) {
//        val date = Calendar.getInstance().time
//        @SuppressLint("SimpleDateFormat") val dateFormat = SimpleDateFormat("yyyy-MM-dd")
        val today = DateTimeUtils.currentDate
        val networkConfig = NetworkConfig()
        compositeDisposable.add(networkConfig.api().getMovieRelease("discover", "movie", BuildConfig.TMDB_API_KEY, today, today)
                .subscribeOn(Schedulers.newThread())
                .subscribe(object : ResponseHandler<MovieResponse>(null) {
                    override fun onSuccess(model: MovieResponse) {
                        movieResults = model.results!!
                        showAlarmNotification(context)
                    }

                    override fun onUnauthorized() {
                        Timber.d("OnUnauthorized ")
                    }

                    override fun onError(model: MovieResponse) {
                        Timber.tag("Error").d(model.message)
                    }

                }, Consumer {
                    if (it is ANError) {
                        Timber.tag("Error").d(it)
                    }
                }))
    }

    private fun getBitmapFromURL(strURL: String): Bitmap? {
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        try {
            val url = URL(strURL)
            val connection = url.openConnection() as HttpURLConnection
            connection.doInput = true
            connection.connect()
            val input = connection.inputStream
            return BitmapFactory.decodeStream(input)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return null
    }

    private fun showAlarmNotification(context: Context) {
        val CHANNEL_ID = "Channel_2"
        val CHANNEL_NAME = "MastseeReleaseReminder channel"

        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        var movies = 0
        try {
            movies = if (movieResults.isNotEmpty()) movieResults.size else 0
        } catch (e: Exception) {
            Timber.tag("ERROR").d(e)
        }

        var msg: String
        if (movies == 0) {
            msg = context.resources.getString(R.string.no_movies_today)

            val builder = NotificationCompat.Builder(context, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_notifications_active_black)
                    .setContentTitle(context.resources.getString(R.string.content_title_release_reminder))
                    .setContentText(msg)
                    .setSubText(context.resources.getString(R.string.release_reminder))
                    .setColor(ContextCompat.getColor(context, android.R.color.transparent))
                    .setVibrate(longArrayOf(1000, 1000, 1000, 1000, 1000))
                    .setSound(alarmSound)

            val intent = Intent(context, MainActivity::class.java)
            val pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

            builder.setContentIntent(pendingIntent)
            builder.setAutoCancel(true)


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT)

                channel.enableVibration(true)
                channel.vibrationPattern = longArrayOf(1000, 1000, 1000, 1000, 1000)
                builder.setChannelId(CHANNEL_ID)
                notificationManager.createNotificationChannel(channel)
            }

            val notification = builder.build()

            notificationManager.notify(DAILY_RELEASE_ID, notification)
        } else {
            for (i in 0 until movies) {
                msg = movieResults[i].title + " " + context.resources.getString(R.string.has_been_release_today)
                val url = "https://image.tmdb.org/t/p/w600_and_h900_bestv2"
                val poster = url +  movieResults[i].backdropPath.replace("\\","")
                val bitmap = getBitmapFromURL(poster)
                val builder = NotificationCompat.Builder(context, CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_notifications_active_black)
                        .setContentTitle(context.resources.getString(R.string.content_title_release_reminder))
                        .setContentText(msg)
                        .setSubText(context.resources.getString(R.string.release_reminder))
                        .setColor(ContextCompat.getColor(context, android.R.color.transparent))
                        .setLargeIcon(bitmap)
                        .setStyle(NotificationCompat.BigPictureStyle().bigPicture(bitmap).bigLargeIcon(null))
                        .setVibrate(longArrayOf(1000, 1000, 1000, 1000, 1000))
                        .setSound(alarmSound)

                val intent = Intent(context, MainActivity::class.java)
                val pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

                builder.setContentIntent(pendingIntent)
                builder.setAutoCancel(true)

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    val channel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT)

                    channel.enableVibration(true)
                    channel.vibrationPattern = longArrayOf(1000, 1000, 1000, 1000, 1000)

                    builder.setChannelId(CHANNEL_ID)
                    notificationManager.createNotificationChannel(channel)
                }

                val notification = builder.build()
                notificationManager.notify(DAILY_RELEASE_ID, notification)
            }
        }
    }

    fun setDailyReminder(context: FragmentActivity?) {
        val alarmManager = context?.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(context, DailyReleaseReceiver::class.java)

        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, 8)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)

        val pendingIntent = PendingIntent.getBroadcast(context, DAILY_RELEASE_ID, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, AlarmManager.INTERVAL_DAY, pendingIntent)
    }

    fun cancelDailyReminder(context: FragmentActivity?) {
        val alarmManager = context?.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(context, DailyReleaseReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(context, DAILY_RELEASE_ID, intent, 0)
        pendingIntent.cancel()

        alarmManager.cancel(pendingIntent)
    }
}
