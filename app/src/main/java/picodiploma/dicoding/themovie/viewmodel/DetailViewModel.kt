package picodiploma.dicoding.themovie.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import picodiploma.dicoding.themovie.data.local.FavoriteMovie
import picodiploma.dicoding.themovie.data.local.repository.FavoriteMovieRepository
import picodiploma.dicoding.themovie.data.local.repository.FavoriteTvRepository
import picodiploma.dicoding.themovie.data.network.NetworkConfig
import picodiploma.dicoding.themovie.data.response.MovieDetailResponse
import picodiploma.dicoding.themovie.data.response.TvDetailResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Iqbal Fauzi on 19:41 13/07/19
 */
class DetailViewModel(
    private val favoriteTvRepository: FavoriteTvRepository,
    private val favoriteMovieRepository: FavoriteMovieRepository
) : ViewModel() {

    private val network: NetworkConfig = NetworkConfig()
    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    val onRequest: MutableLiveData<Boolean> = MutableLiveData()

    val tvDetailData: MutableLiveData<TvDetailResponse> = MutableLiveData()
    val movieDetailData: MutableLiveData<MovieDetailResponse> = MutableLiveData()
    val message: MutableLiveData<String> = MutableLiveData()
    val handleError: MutableLiveData<Int> = MutableLiveData()

    fun setFavoriteMovie(favoriteMovie: FavoriteMovie) {
        compositeDisposable.add(Observable.fromCallable {
            favoriteMovieRepository.saveMovie(
                favoriteMovie
            )
        }
            .subscribeOn(Schedulers.computation())
            .subscribe())
    }

    fun removeFavoriteMovie(movie: FavoriteMovie) {
        compositeDisposable.add(Observable.fromCallable { favoriteMovieRepository.deleteMovie(movie) }
            .subscribeOn(Schedulers.computation())
            .subscribe())
    }

    fun getTvDetail(tvId: String): LiveData<TvDetailResponse> {
        onRequest.postValue(true)
        network.api().getDetailTv(tvId).enqueue(object : Callback<TvDetailResponse> {
            override fun onFailure(call: Call<TvDetailResponse>, t: Throwable) {
                onRequest.value = false
                message.value = t.message
            }

            override fun onResponse(
                call: Call<TvDetailResponse>,
                response: Response<TvDetailResponse>
            ) {
                if (response.isSuccessful) {
                    onRequest.value = false
                    tvDetailData.value = response.body()
                } else {
                    onRequest.value = false
                    message.value = response.body()?.message
                }
            }

        })

        return tvDetailData
    }

    fun getMovieDetail(movieId: String): LiveData<MovieDetailResponse> {
        onRequest.postValue(true)
        network.api().getDetailMovie(movieId).enqueue(object : Callback<MovieDetailResponse> {
            override fun onFailure(call: Call<MovieDetailResponse>, t: Throwable) {
                onRequest.value = false
                message.value = t.message
            }

            override fun onResponse(
                call: Call<MovieDetailResponse>,
                response: Response<MovieDetailResponse>
            ) {
                if (response.isSuccessful) {
                    onRequest.value = false
                    movieDetailData.value = response.body()
                } else {
                    onRequest.value = false
                    message.value = response.body()?.message
                }
            }

        })

        return movieDetailData
    }

    override fun onCleared() {
        compositeDisposable.dispose()
    }

}