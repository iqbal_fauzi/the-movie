package picodiploma.dicoding.themovie.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import picodiploma.dicoding.themovie.data.local.FavoriteMovie
import picodiploma.dicoding.themovie.data.local.FavoriteTv
import picodiploma.dicoding.themovie.data.local.repository.FavoriteMovieRepository
import picodiploma.dicoding.themovie.data.local.repository.FavoriteTvRepository

/**
 * Created by Iqbal Fauzi on 19:41 13/07/19
 */
class FavoriteViewModel(private val favoriteTvRepository: FavoriteTvRepository, private val favoriteMovieRepository: FavoriteMovieRepository) :
    ViewModel() {

    var gson = Gson()
    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    val onRequestData: MutableLiveData<Boolean> = MutableLiveData()
    val favoriteMovieData: MutableLiveData<List<FavoriteMovie>> = MutableLiveData()
    val favoriteTvData: MutableLiveData<List<FavoriteTv>> = MutableLiveData()

    fun getAllFavouriteMovie(): LiveData<List<FavoriteMovie>> {
        onRequestData.postValue(true)
        compositeDisposable.add(Observable.fromCallable {
            favoriteMovieData.postValue(favoriteMovieRepository.getAllMovies())
        }
            .subscribeOn(Schedulers.computation())
            .subscribe())
        onRequestData.postValue(false)
        return favoriteMovieData
    }

    fun setFavoriteMovie(favoriteMovie: FavoriteMovie) {
        compositeDisposable.add(Observable.fromCallable { favoriteMovieRepository.saveMovie(favoriteMovie) }
            .subscribeOn(Schedulers.computation())
            .subscribe())
        getAllFavouriteMovie()
    }

    fun removeFavoriteMovie(movie: FavoriteMovie) {
        compositeDisposable.add(Observable.fromCallable { favoriteMovieRepository.deleteMovie(movie) }
            .subscribeOn(Schedulers.computation())
            .subscribe())
        getAllFavouriteMovie()
    }

    fun getAllFavoritTv(): LiveData<List<FavoriteTv>> {
        onRequestData.postValue(true)
        compositeDisposable.add(Observable.fromCallable {
            favoriteTvData.postValue(favoriteTvRepository.getAllTvShow())
        }
            .subscribeOn(Schedulers.computation())
            .subscribe())
        onRequestData.postValue(false)
        return favoriteTvData
    }

    fun setFavoriteTvShow(favoriteTv: FavoriteTv) {
        compositeDisposable.add(Observable.fromCallable { favoriteTvRepository.saveTv(favoriteTv) }
            .subscribeOn(Schedulers.computation())
            .subscribe())
        getAllFavoritTv()
    }

    fun removeFavoriteTvShow(tv: FavoriteTv) {
        compositeDisposable.add(Observable.fromCallable { favoriteTvRepository.deleteTv(tv) }
            .subscribeOn(Schedulers.computation())
            .subscribe())
        getAllFavoritTv()
    }

    override fun onCleared() {
        compositeDisposable.dispose()
    }

}