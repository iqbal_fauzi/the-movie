package picodiploma.dicoding.themovie.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import picodiploma.dicoding.themovie.BuildConfig
import picodiploma.dicoding.themovie.data.local.FavoriteMovie
import picodiploma.dicoding.themovie.data.local.FavoriteTv
import picodiploma.dicoding.themovie.data.local.repository.FavoriteMovieRepository
import picodiploma.dicoding.themovie.data.local.repository.FavoriteTvRepository
import picodiploma.dicoding.themovie.data.network.ApiError
import picodiploma.dicoding.themovie.data.network.NetworkConfig
import picodiploma.dicoding.themovie.data.network.ResponseHandler
import picodiploma.dicoding.themovie.data.response.MovieResponse
import picodiploma.dicoding.themovie.data.response.TvResponse
import picodiploma.dicoding.themovie.util.AppLogger
import picodiploma.dicoding.themovie.util.CommonUtils

/**
 * Created by Iqbal Fauzi on 19:41 13/07/19
 */
class SearchViewModel(private val favoriteMovieRepository: FavoriteMovieRepository, private val favoriteTvRepository: FavoriteTvRepository) :
        ViewModel() {

    private val network: NetworkConfig = NetworkConfig()
    var gson = Gson()
    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    val onRequestData: MutableLiveData<Boolean> = MutableLiveData()
    val movieData: MutableLiveData<List<MovieResponse.Movies>> = MutableLiveData()
    val tvData: MutableLiveData<List<TvResponse.TvShow>> = MutableLiveData()

    private val handleError: MutableLiveData<Int> = MutableLiveData()
    private val message: MutableLiveData<String> = MutableLiveData()

    fun setFavoriteTvShow(favoriteTv: FavoriteTv) {
        compositeDisposable.add(Observable.fromCallable { favoriteTvRepository.saveTv(favoriteTv) }
            .subscribeOn(Schedulers.computation())
            .subscribe())
    }

    fun removeFavoriteTvShow(tv: FavoriteTv) {
        compositeDisposable.add(Observable.fromCallable { favoriteTvRepository.deleteTv(tv) }
            .subscribeOn(Schedulers.computation())
            .subscribe())
    }

    fun setFavoriteMovie(favoriteMovie: FavoriteMovie) {
        compositeDisposable.add(Observable.fromCallable { favoriteMovieRepository.saveMovie(favoriteMovie) }
                .subscribeOn(Schedulers.computation())
                .subscribe())
    }

    fun removeFavoriteMovie(movie: FavoriteMovie) {
        compositeDisposable.add(Observable.fromCallable { favoriteMovieRepository.deleteMovie(movie) }
                .subscribeOn(Schedulers.computation())
                .subscribe())
    }

    fun findTv(tvName: String): LiveData<List<TvResponse.TvShow>> {
        onRequestData.postValue(true)
        compositeDisposable.add(network.api().findTv(tvName)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ResponseHandler<TvResponse>(null) {
                override fun onSuccess(model: TvResponse) {
                    onRequestData.value = false
                    tvData.value = model.results
                }

                override fun onUnauthorized() {
                    onRequestData.value = false
                }

                override fun onError(model: TvResponse) {
                    logging(model.message)
                    onRequestData.value = false
                }
            }, Consumer {
                if (it is ANError) {
                    handleApiError(it)
                    onRequestData.value = false
                }
            }))
        return tvData
    }

    fun findMovie(movieName: String): LiveData<List<MovieResponse.Movies>> {
        onRequestData.postValue(true)
        compositeDisposable.add(network.api().findMovies(movieName)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ResponseHandler<MovieResponse>(null) {
                override fun onSuccess(model: MovieResponse) {
                    onRequestData.value = false
                    movieData.value = model.results
                }

                override fun onUnauthorized() {
                    onRequestData.value = false
                }

                override fun onError(model: MovieResponse) {
                    logging(model.message)
                    onRequestData.value = false
                }
            }, Consumer {
                if (it is ANError) {
                    handleApiError(it)
                    onRequestData.value = false
                }
            })
        )
        return movieData
    }

    private fun handleApiError(error: ANError) {
        if (CommonUtils.isJSONValid(error.errorBody)) {
            val apiError = gson.fromJson(error.errorBody, ApiError::class.java)

            if (apiError != null) {
                if (error.errorCode == 401) {
                    message.postValue(apiError.errors?.get(0)?.error)
                } else {
                    message.postValue(apiError.errors?.get(0)?.error)
                }
            } else {
                message.postValue(error.message)
            }
        } else {
            if (error.toString().contains("UnknownHost")) {
                handleError.postValue(1)
            } else if (error.toString().contains("timed out") || error.toString().contains("timeout")) {
                handleError.postValue(2)
            } else if (error.toString().contains("connectionError") || error.toString().contains("connectionError")) {
                handleError.postValue(3)
            } else if (error.errorBody != null) {
                if (error.errorBody.contains("html") || error.errorBody.contains("java")) {
                    handleError.postValue(3)
                } else {
                    handleError.postValue(4)
                }
            } else {
                handleError.postValue(4)
            }
        }
    }

    fun logging(message: String?) {
        if (message != null) {
            if (BuildConfig.DEBUG) {
                AppLogger.i(message)
                this.message.postValue(message)
            }
        }
    }

    override fun onCleared() {
        compositeDisposable.dispose()
    }

}